#pragma once

#include <string_view>
#include <optional>
#include <vector>
#include <utility>
#include <array>

#include "shapes/shape.hpp"
#include "camera.hpp"

struct image_size_t
{
    int nx, ny;
};

struct configuration
{
    int max_depth = 50; // maximum recursive depth for ray_color
    int samples_per_pixel = 20; // for anti-aliasing
    int pixels_per_process = 1; // number of pixels to grab/write for each communication.
    image_size_t image_size;
    camera cam;
    color3 background_color = color3{0.7f, 0.8f, 1.0f};
    std::vector<shape> shapes;

    configuration() = default;
};

std::optional<configuration> input(std::string_view input_file);

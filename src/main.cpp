#include <mpi.h>

#include <glm/geometric.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/compatibility.hpp>

#include "bounding_volumes/bvh.hpp"
#include "camera.hpp"
#include "core/ray.hpp"
#include "input.hpp"
#include "shapes/shape.hpp"
#include "stb_write.hpp"
#include <cppcoro/generator.hpp>
#include <ctime>
#include <limits>
#include <optional>
#include "random.hpp"

#include <fmt/core.h>
using namespace std::literals;

// background blend
constexpr auto bottom_color = color3(1.0f, 1.0f, 1.0f); // white
constexpr auto top_color = color3(0.5f, 0.7f, 1.0f);    // light blue

constexpr real inf = std::numeric_limits<real>::infinity();

// set the color of the ray based on y position and top/bottom colors
color3
ray_color(const ray& r, const color3& background, const bvh& volume, int depth)
{
    if (depth <= 0) return {0, 0, 0}; // black

    if (auto hit = volume.hit(r, 1e-6, inf); hit) {
        const material& mat = hit->mat;
        auto emitted = mat.emitted(hit->u, hit->v, hit->point);

        if (auto s = mat.scatter(r, *hit); s) {
            return emitted + s->attenuation * ray_color(s->scattered, background, volume, depth - 1);
        } else
            return emitted;
    }
    // auto t = 0.5f * (glm::normalize(r.direction).y + 1.0f);
    // return glm::lerp(bottom_color, top_color, t);
    return background;
}

struct pixel_data {
    int i;
    int j;
    color3 color;
};

struct image_index_t {
    int i;
    int j;
};

cppcoro::generator<image_index_t> image_indices(int nx, int ny, int pixels_per_process)
{
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Win w;
    int* base;
    if (rank == 0) {
        int* base;
        MPI_Win_allocate(sizeof(int), 1, MPI_INFO_NULL, MPI_COMM_WORLD, &base, &w);
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, w);
        *base = 0;
        MPI_Win_unlock(0, w);
    } else {
        MPI_Win_allocate(0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &base, &w);
    }

    int n = -1;
    const int sz = nx * ny;
    // int increment = pixels_per_process;
    while (n < sz) {
        MPI_Win_lock(MPI_LOCK_SHARED, 0, 0, w);
        MPI_Fetch_and_op(&pixels_per_process, &n, MPI_INT, 0, 0, MPI_SUM, w);
        MPI_Win_unlock(0, w);

        for (int k = 0; k < pixels_per_process && n < sz; k++, n++) {
            int j = n / nx;
            int i = n - j * nx;
            // fmt::print("rank {:2d} is processing ({:3d}, {:3d})\n", rank, i, j);
            co_yield {i, j};
        }
    }
    MPI_Win_free(&w);
}

cppcoro::generator<pixel_data> render_image(const configuration& config)
{
    const auto& cam = config.cam;
    const auto& shapes = config.shapes;
    auto [nx, ny] = config.image_size;
    const int nsamples = config.samples_per_pixel;
    const int max_depth = config.max_depth;
    auto background = config.background_color;
    bvh volume{shapes, cam.time_range()};

    for (auto&& [i, j] : image_indices(nx, ny, config.pixels_per_process)) {
        color3 color{0, 0, 0};
        for (int n = 0; n < nsamples; n++) {
            auto u = (i + pick()) / (nx - 1);
            auto v = (j + pick()) / (ny - 1);
            color += ray_color(cam.ray_to(u, v), background, volume, max_depth);
        }
        // fmt::print("point ({:3d}, {:3d}) has been processed\n", i, j);
        co_yield pixel_data{i, j, color / real(nsamples)};
    }
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    std::string_view input_file = argc > 1 ? argv[1] : "input.lua"sv;

    auto config = input(input_file);
    if (!config) MPI_Abort(MPI_COMM_WORLD, 1);

    auto [nx, ny] = config->image_size;
    stb_png_writer png{nx, ny, config->pixels_per_process};

    // prepare for randomness
    std::srand(std::time(nullptr));
    randomize();

    for (auto&& [i, j, color] : render_image(*config)) { png.pixel(i, j) = color; }

    png.write("test.png"sv);

    MPI_Finalize();
}

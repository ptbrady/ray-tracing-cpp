#pragma once

#include <glm/geometric.hpp>
#include "ray.hpp"
#include <functional>

class material;

struct hit_record {
    real t;
    real u, v;       // surface coordinates of object
    bool ray_outside; // true if ray is incoming from outside the object
    point3 point;
    vec3 normal;                           // outward normal if ray_outside
    std::reference_wrapper<const material> mat; // material we hit.

    hit_record(const ray& r,
               real t,
               real u,
               real v,
               const point3& point,
               const vec3& outward_normal,
               const material& mat)
        : t{t},
          u{u},
          v{v},
          ray_outside{glm::dot(r.direction, outward_normal) < 0},
          point{point},
          normal{ray_outside ? outward_normal : -outward_normal},
          mat{mat}
    {
    }
};

struct scatter_record {
    vec3 attenuation;
    ray scattered;
};

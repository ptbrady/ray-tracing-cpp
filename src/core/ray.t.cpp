#include "ray.hpp"
#include <catch2/catch_test_macros.hpp>

TEST_CASE("Ray")
{
    ray r{.origin = vec3{1, 2, 3}, .direction = vec3{-2, -1, -2}};

    REQUIRE(r.position(1.0) == vec3{-1, 1, 1});   
}

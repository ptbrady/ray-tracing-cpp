#pragma once

#include "../floating_types.hpp"
#include "../vector_types.hpp"

struct ray {
    point3 origin;
    vec3 direction;
    real time;

    constexpr point3 position(real t) const { return origin + t * direction; }

    constexpr ray translate(const vec3& offset) const
    {
        return {origin - offset, direction, time};
    }
};

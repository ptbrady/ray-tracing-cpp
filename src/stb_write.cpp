#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image/stb_image_write.h"

#include "stb_write.hpp"
#include <cassert>
#include <fmt/core.h>
#include <mpi.h>

/*
 * Use `pixels_per_process` to buffer the getting and putting of data
 * The write buffering requires that we hold a queue of data points that
 * are referenced in the png_pixel class and the write only happens when:
 * 1) The last buffered pixel is assigned or
 * 2) No more pixels will be assigned
 * This logic will be moved to operator= and is only correct for contiguously
 * buffered pixels.  Relaxing this constraint would allow for more dynamic and different
 * buffering on getting and putting
 */

stb_png_writer::stb_png_writer(int nx, int ny, int pixels_per_process)
    : nx{nx}, ny{ny}, pixels_per_process{pixels_per_process}
{
    buffered_pixels.reserve(pixels_per_process);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    pixel_t* base;

    if (rank == 0) {
        MPI_Win_allocate(sizeof(pixel_t) * nx * ny,
                         sizeof(pixel_t),
                         MPI_INFO_NULL,
                         MPI_COMM_WORLD,
                         &base,
                         &w);
        image = std::span(base, nx * ny);

        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, w);
        for (auto&& v : image) v = {0, 0, 0};
        MPI_Win_unlock(0, w);
    } else {
        MPI_Win_allocate(0, 1, MPI_INFO_NULL, MPI_COMM_WORLD, &base, &w);
    }
}

void stb_png_writer::print_pixel(int i, int j) const
{
    if (rank == 0) {
        pixel_t& p = image[j * nx + i];
        fmt::print("   {u3}, {u3}, {u3}\n", p[0], p[1], p[2]);
    }
}

void stb_png_writer::write(std::string_view filename) const
{
    // force a correct view on the data before writing
    MPI_Barrier(MPI_COMM_WORLD);
    if (rank == 0) {
        MPI_Win_lock(MPI_LOCK_EXCLUSIVE, 0, 0, w);
        MPI_Win_unlock(0, w);
        stbi_flip_vertically_on_write(1);
        stbi_write_png(filename.data(), nx, ny, 3, &image[0], nx * 3);
    }
}

// This routine is not idempotent,  do *NOT* call this routine with the
// same (i,j) more than once!!!
png_pixel stb_png_writer::pixel(int i, int j)
{
    assert(i < nx && j < ny);
    int last_pixel = nx * ny - 1;
    int offset = j * nx + i;
    int npixels = pixels_per_process - buffered_pixels.size();

    return {offset - static_cast<int>(buffered_pixels.size()),
            offset == last_pixel || npixels == 1,
            buffered_pixels,
            w};
}

png_pixel& png_pixel::operator=(const color3& v)
{
    auto c = glm::clamp(glm::sqrt(v), real(0), real(1));
    buffered_pixels.push_back({static_cast<unsigned char>(255.9999 * c.r),
                               static_cast<unsigned char>(255.9999 * c.g),
                               static_cast<unsigned char>(255.9999 * c.b)});
    if (write) {
        MPI_Win_lock(MPI_LOCK_SHARED, 0, 0, w);
        MPI_Accumulate(&buffered_pixels[0],
                       3 * buffered_pixels.size(),
                       MPI_CHAR,
                       0,
                       offset,
                       3 * buffered_pixels.size(),
                       MPI_CHAR,
                       MPI_REPLACE,
                       w);
        MPI_Win_unlock(0, w);
        buffered_pixels.clear();
    }

    return *this;
}

#pragma once

#include "tag_invoke.hpp"
#include <cassert>
#include <concepts>
#include <cstddef>
#include <cstring>
#include <type_traits>
#include <vector>

namespace bytes
{

template <typename T>
concept Bytes = std::is_trivially_copyable_v<T>;

// Simple archiver reads and writes trivially copyable objects only
// The above CPOS must be used to read/write other objects.
class archiver;

namespace cpo
{
// define some CPOS to be used by objects requiring special handling
inline constexpr struct to_bytes_fn {
    template <typename T>
    auto operator()(archiver& ar, const T& obj) const
        -> taggie::tag_invoke_result_t<to_bytes_fn, archiver&, const T&>
    {
        return taggie::tag_invoke(*this, ar, obj);
    }
} to_bytes{};

inline constexpr struct to_obj_fn {
    template <typename T>
    auto operator()(archiver& ar, T& obj) const
        -> taggie::tag_invoke_result_t<to_obj_fn, archiver&, T&>
    {
        return taggie::tag_invoke(*this, ar, obj);
    }
} to_obj{};

} // namespace cpo

class archiver
{
    std::vector<std::byte> ar; // main archive
    int read_index;

public:
    archiver() = default;
    archiver(int n) : ar(n), read_index{0} {}

    operator bool() const { return read_index < static_cast<int>(ar.size()); }

    auto size() const { return ar.size(); }

    auto data() { return ar.data(); }
    auto data() const { return ar.data(); }

private:
    template <Bytes T>
    void write(const T& t)
    {
        constexpr auto sz = sizeof(T);
        std::byte b[sz];
        std::memcpy(static_cast<void*>(b), static_cast<const void*>(&t), sz);
        ar.insert(ar.end(), &b[0], &b[sz]);
    }

    template <Bytes T>
    void write(const std::vector<T>& t)
    {
        auto vec_sz = t.size();
        write(vec_sz);

        // resize byte archive so we can write our bytes using memcpy
        auto write_sz = vec_sz * sizeof(T);
        auto ar_sz = ar.size();
        ar.resize(ar_sz + write_sz);

        std::memcpy(static_cast<void*>(&ar[ar_sz]), static_cast<const void*>(&t[0]), write_sz);
    }

    template <Bytes T>
    void read(T& t)
    {
        constexpr auto sz = sizeof(T);
        assert(read_index + sz <= ar.size());

        std::memcpy(
            static_cast<void*>(&t), static_cast<const void*>(ar.data() + read_index), sz);
        // move record foward
        read_index += sz;
    }

    template <Bytes T>
    void read(std::vector<T>& t)
    {
        auto sz = t.size();
        read(sz);

        t.resize(sz);
        // number of bytes to be read
        sz *= sizeof(T);
        assert(read_index + sz <= ar.size());
        std::memcpy(static_cast<void*>(t.data()),
                    static_cast<const void*>(ar.data() + read_index),
                    sz);
        read_index += sz;
    }

    // default impl for bytes types
    template <Bytes T>
    friend void tag_invoke(taggie::tag_t<cpo::to_bytes>, archiver& ar, const T& obj)
    {
        ar.write(obj);
    }

    template <Bytes T>
    friend void tag_invoke(taggie::tag_t<cpo::to_obj>, archiver& ar, T& t)
    {
        ar.read(t);
    }

    // default impl for vector of bytes types
    template <Bytes T>
    friend void
    tag_invoke(taggie::tag_t<cpo::to_bytes>, archiver& ar, const std::vector<T>& obj)
    {
        ar.write(obj);
    }

    template <Bytes T>
    friend void tag_invoke(taggie::tag_t<cpo::to_obj>, archiver& ar, std::vector<T>& obj)
    {
        ar.read(obj);
    }
};

// everyone will call these functions
template <typename T>
requires taggie::is_tag_invocable_v<decltype(cpo::to_bytes), archiver&, const T&> void
to_bytes(archiver& ar, const T& t)
{
    cpo::to_bytes(ar, t);
}

template <typename T>
requires taggie::is_tag_invocable_v<decltype(cpo::to_obj), archiver&, T&>
    T to_obj(archiver& ar)
{
    T t{};
    cpo::to_obj(ar, t);
    return t;
}

} // namespace bytes

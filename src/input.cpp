#include "input.hpp"

#include <filesystem>
#include <fmt/core.h>
#include <mpi.h>
#include <sol/sol.hpp>

#include "shapes/shapes.hpp"

namespace fs = std::filesystem;
// serializer for configuration object
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const configuration& c)
{
    bytes::to_bytes(ar, c.max_depth);
    bytes::to_bytes(ar, c.samples_per_pixel);
    bytes::to_bytes(ar, c.pixels_per_process);
    bytes::to_bytes(ar, c.image_size);
    bytes::to_bytes(ar, c.cam);
    bytes::to_bytes(ar, c.background_color);
    bytes::to_bytes(ar, c.shapes);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, configuration& c)
{
    c.max_depth = bytes::to_obj<int>(ar);
    c.samples_per_pixel = bytes::to_obj<int>(ar);
    c.pixels_per_process = bytes::to_obj<int>(ar);
    c.image_size = bytes::to_obj<decltype(c.image_size)>(ar);
    c.cam = bytes::to_obj<camera>(ar);
    c.background_color = bytes::to_obj<color3>(ar);
    c.shapes = bytes::to_obj<std::vector<shape>>(ar);
}

// functions for image_size
static configuration& image_size_from_int(configuration& self, int nx, int ny)
{
    self.image_size.nx = nx;
    self.image_size.ny = ny;
    return self;
}

static configuration& image_size_from_tbl(configuration& self, sol::table tbl)
{
    if (sol::optional<int> nx = tbl["nx"]; nx) {
        self.image_size.nx = *nx;
    } else {
        throw std::runtime_error("image_size:nx must be a valid integer");
    }

    if (sol::optional<int> ny = tbl["ny"]; ny) {
        self.image_size.ny = *ny;
    } else {
        throw std::runtime_error("image_size:ny must be a valid integer");
    }
    return self;
}

static configuration& add_shape(configuration& self, const shape& s)
{
    self.shapes.push_back(s);
    return self;
}

//
//
static shape make_sphere_(sol::table tbl)
{
    sol::optional<point3> center = tbl["center"];
    sol::optional<real> radius = tbl["radius"];
    sol::optional<material> mat = tbl["material"];

    if (!center)
        throw std::runtime_error("add_sphere.center must be a valid vec3 object");
    if (!radius) throw std::runtime_error("add_sphere.radius must be a valid real");
    if (!mat) throw std::runtime_error("add_sphere.material must be a valid material");

    return make_sphere(*center, *radius, *mat);
}

static configuration& add_sphere(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_sphere_(tbl));
    return self;
}

//
//
static shape make_xy_rect_(sol::table tbl)
{
    sol::optional<point2> lower_corner = tbl["lower_corner"];
    sol::optional<point2> upper_corner = tbl["upper_corner"];
    sol::optional<real> coordinate = tbl["plane_coordinate"];
    sol::optional<material> mat = tbl["material"];

    if (!lower_corner)
        throw std::runtime_error("add_xy_rect.lower_corner must be a valid vec2 object");
    if (!upper_corner)
        throw std::runtime_error("add_xy_rect.upper_corner must be a valid vec2 object");
    if (!coordinate)
        throw std::runtime_error("add_xy_rect.coordinate must be a valid real");
    if (!mat) throw std::runtime_error("add_xy_rect.material must be a valid material");

    return make_xy_rect(*lower_corner, *upper_corner, *coordinate, *mat);
}

configuration& add_xy_rect(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_xy_rect_(tbl));
    return self;
}

//
//
static shape make_xz_rect_(sol::table tbl)
{
    sol::optional<point2> lower_corner = tbl["lower_corner"];
    sol::optional<point2> upper_corner = tbl["upper_corner"];
    sol::optional<real> coordinate = tbl["plane_coordinate"];
    sol::optional<material> mat = tbl["material"];

    if (!lower_corner)
        throw std::runtime_error("add_xz_rect.lower_corner must be a valid vec2 object");
    if (!upper_corner)
        throw std::runtime_error("add_xz_rect.upper_corner must be a valid vec2 object");
    if (!coordinate)
        throw std::runtime_error("add_xz_rect.coordinate must be a valid real");
    if (!mat) throw std::runtime_error("add_xz_rect.material must be a valid material");

    return make_xz_rect(*lower_corner, *upper_corner, *coordinate, *mat);
}

static configuration& add_xz_rect(configuration& self, sol::table tbl)
{

    self.shapes.push_back(make_xz_rect_(tbl));
    return self;
}

//
//
static shape make_yz_rect_(sol::table tbl)
{
    sol::optional<point2> lower_corner = tbl["lower_corner"];
    sol::optional<point2> upper_corner = tbl["upper_corner"];
    sol::optional<real> coordinate = tbl["plane_coordinate"];
    sol::optional<material> mat = tbl["material"];

    if (!lower_corner)
        throw std::runtime_error("add_yz_rect.lower_corner must be a valid vec2 object");
    if (!upper_corner)
        throw std::runtime_error("add_yz_rect.upper_corner must be a valid vec2 object");
    if (!coordinate)
        throw std::runtime_error("add_yz_rect.coordinate must be a valid real");
    if (!mat) throw std::runtime_error("add_yz_rect.material must be a valid material");

    return make_yz_rect(*lower_corner, *upper_corner, *coordinate, *mat);
}

static configuration& add_yz_rect(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_yz_rect_(tbl));
    return self;
}

//
//

static shape make_box_(sol::table tbl)
{
    sol::optional<point3> lower_corner = tbl["lower_corner"];
    sol::optional<point3> upper_corner = tbl["upper_corner"];
    sol::optional<material> mat = tbl["material"];

    if (!lower_corner)
        throw std::runtime_error("add_box.lower_corner must be a valid vec3 object");
    if (!upper_corner)
        throw std::runtime_error("add_box.upper_corner must be a valid vec3 object");
    if (!mat) throw std::runtime_error("add_box.material must be a valid material");

    return make_box(*lower_corner, *upper_corner, *mat);
}

static configuration& add_box(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_box_(tbl));
    return self;
}

//
//
static shape make_moving_sphere_(sol::table tbl)
{
    sol::optional<point3> center_a = tbl["center_a"];
    sol::optional<point3> center_b = tbl["center_b"];
    sol::optional<real> time_a = tbl["time_a"];
    sol::optional<real> time_b = tbl["time_b"];
    sol::optional<real> radius = tbl["radius"];
    sol::optional<material> mat = tbl["material"];

    if (!center_a)
        throw std::runtime_error(
            "add_moving_sphere.center_a must be a valid vec3 object");
    if (!center_b)
        throw std::runtime_error(
            "add_moving_sphere.center_b must be a valid vec3 object");
    if (!time_a)
        throw std::runtime_error("add_moving_sphere.time_a must be a valid real");
    if (!time_b)
        throw std::runtime_error("add_moving_sphere.time_b must be a valid real");
    if (!radius)
        throw std::runtime_error("add_moving_sphere.radius must be a valid real");
    if (!mat)
        throw std::runtime_error("add_moving_sphere.material must be a valid material");

    return make_moving_sphere(*center_a, *center_b, *time_a, *time_b, *radius, *mat);
}

static configuration& add_moving_sphere(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_moving_sphere_(tbl));
    return self;
}

static shape make_translate_shape_(sol::table tbl)
{
    sol::optional<shape> shape_ = tbl["shape"];
    sol::optional<vec3> displacement = tbl["displacement"];

    if (!shape_) throw std::runtime_error("translate_shape.shape must be a valid shape");
    if (!displacement)
        throw std::runtime_error("translate_shape.displacement must be a valid vec3");
    return translate_shape(*shape_, *displacement);
}

static configuration& add_translate_shape(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_translate_shape_(tbl));
    return self;
}

//
//

static shape make_rotate_shape_(sol::table tbl)
{
    sol::optional<shape> shape_ = tbl["shape"];
    sol::optional<real> angle = tbl["angle"];
    sol::optional<char> axis = tbl["axis"];

    if (!shape_) throw std::runtime_error("rotate_shape.shape must be a valid shape");
    if (!angle) throw std::runtime_error("rotate_shape.angle must be a valid angle");
    if (!axis) throw std::runtime_error("rotate_shape.axis must be on of 'x', 'y', 'z'");

    if (*axis == 'x')
        return rotate_x_shape(*shape_, *angle);
    if (*axis == 'y')
        return rotate_y_shape(*shape_, *angle);
    if (*axis == 'z')
        return rotate_z_shape(*shape_, *angle);

    throw std::runtime_error("rotate_shape.axis must be on of 'x', 'y', 'z'");
}

static configuration& add_rotate_shape(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_rotate_shape_(tbl));
    return self;
}

//
//

static shape make_constant_medium_(sol::table tbl)
{
    sol::optional<shape> shape_ = tbl["shape"];
    sol::optional<texture> texture = tbl["texture"];
    sol::optional<real> density = tbl["density"];

    if (!shape_) throw std::runtime_error("constant_medium.shape must be a valid shape");
    if (!texture) throw std::runtime_error("constant_medium.texture must be a valid texture");
    if (!density) throw std::runtime_error("constant_medium.density must be a valid real");

    return make_constant_medium(*shape_, *texture, *density);
}

static configuration& add_constant_medium(configuration& self, sol::table tbl)
{
    self.shapes.push_back(make_constant_medium_(tbl));
    return self;
}

// functions for creating camera
static configuration& set_camera(configuration& self, sol::table tbl)
{
    sol::optional<real> vfov = tbl["vertical_field_of_view"];
    sol::optional<real> aspect_ratio = tbl["aspect_ratio"];
    sol::optional<point3> look_from = tbl["look_from"];
    sol::optional<point3> look_at = tbl["look_at"];
    sol::optional<vec3> view_up = tbl["view_up"];
    sol::optional<real> aperature = tbl["aperature"];
    sol::optional<real> focus_dist = tbl["focus_distance"];
    real time_min = tbl["time_min"].get_or(0.0f);
    real time_max = tbl["time_max"].get_or(0.0f);

    if (!vfov)
        throw std::runtime_error(
            "camera.vertical_field_of_view must be a valid real (angle)");
    if (!aspect_ratio)
        throw std::runtime_error("camera.aspect_ratio must be a valid real");
    if (!look_from) throw std::runtime_error("camera.look_from must me a valid point");
    if (!look_at) throw std::runtime_error("camera.look_at must be a valid point");
    if (!view_up) throw std::runtime_error("camera.view_up must be a valid vec3");
    if (!aperature) throw std::runtime_error("camera.aperature must be a valid real");
    if (!focus_dist) throw std::runtime_error("camera.focus_dist must be a valid real");

    self.cam = camera(*look_from,
                      *look_at,
                      *view_up,
                      *vfov,
                      *aspect_ratio,
                      *aperature,
                      *focus_dist,
                      time_min,
                      time_max);
    return self;
}

std::optional<configuration> input_(std::string_view input_file)
{
    if (!fs::exists(input_file)) {
        fmt::print(stderr, "configuration file '{}' does not exist.\n", input_file);
        return std::nullopt;
    }

    sol::state lua;
    // we could restrict this call but I'm not sure why we would
    lua.open_libraries();

    {
        sol::usertype<vec3> v3 = lua.new_usertype<vec3>(
            "glm_vec3",
            sol::constructors<vec3(real, real, real)>(),
            sol::meta_function::addition,
            sol::resolve<vec3(const vec3&, const vec3&)>(glm::operator+),
            sol::meta_function::subtraction,
            sol::resolve<vec3(const vec3&, const vec3&)>(glm::operator+),
            sol::meta_function::multiplication,
            sol::overload(sol::resolve<vec3(const vec3&, const vec3&)>(glm::operator*),
                          sol::resolve<vec3(real, const vec3&)>(glm::operator*),
                          sol::resolve<vec3(const vec3&, real)>(glm::operator*)),
            "distance",
            sol::resolve<real(const vec3&, const vec3&)>(glm::distance),
            "length",
            sol::resolve<real(const vec3&)>(glm::length));
        // register vec3 object function
        lua["vec"] = lua["glm_vec3"]["new"];
        lua["vec3"] = lua["vec"];
        lua["color"] = lua["vec"];
        lua["point"] = lua["vec"];
    }

    {
        sol::usertype<vec2> v2 = lua.new_usertype<vec2>(
            "glm_vec2",
            sol::constructors<vec2(real, real)>(),
            sol::meta_function::addition,
            sol::resolve<vec2(const vec2&, const vec2&)>(glm::operator+),
            sol::meta_function::subtraction,
            sol::resolve<vec2(const vec2&, const vec2&)>(glm::operator+),
            sol::meta_function::multiplication,
            sol::overload(sol::resolve<vec2(const vec2&, const vec2&)>(glm::operator*),
                          sol::resolve<vec2(real, const vec2&)>(glm::operator*),
                          sol::resolve<vec2(const vec2&, real)>(glm::operator*)),
            "distance",
            sol::resolve<real(const vec2&, const vec2&)>(glm::distance),
            "length",
            sol::resolve<real(const vec2&)>(glm::length));
        // register vec2 object function
        lua["vec2"] = lua["glm_vec2"]["new"];
        lua["point2"] = lua["vec2"];
    }

    lua["solid_color"] =
        sol::overload([](const color3& color) { return make_solid_color(color); },
                      [](real x, real y, real z) {
                          return make_solid_color(color3{x, y, z});
                      });

    lua["checkered"] = [](const texture& even, const texture& odd) {
        return make_checkered(even, odd);
    };

    lua["noise"] = [](int n, real scale) { return make_noise(n, scale); };
    lua["image"] = [](const char* filename) { return make_image(filename); };

    // register material functions
    lua["lambertian"] = [](const texture& tex) { return make_lambertian(tex); };
    lua["metal"] = [](const color3& albedo, real fuzz) {
        return make_metal(albedo, fuzz);
    };
    lua["dielectric"] = [](real index_of_refraction) {
        return make_dielectric(index_of_refraction);
    };
    lua["diffuse_light"] = [](const color3& color) { return make_diffuse_light(color); };
    // register configuration object
    {
        sol::usertype<configuration> config = lua.new_usertype<configuration>(
            "configuration",
            sol::constructors<configuration()>(),
            "image_size",
            sol::overload(image_size_from_int, image_size_from_tbl),
            "add_shape",
            add_shape,
            "add_sphere",
            add_sphere,
            "add_moving_sphere",
            add_moving_sphere,
            "add_xy_rect",
            add_xy_rect,
            "add_xz_rect",
            add_xz_rect,
            "add_yz_rect",
            add_yz_rect,
            "add_box",
            add_box,
            "add_translate_shape",
            add_translate_shape,
            "add_rotate_shape",
            add_rotate_shape,
            "add_constant_medium",
            add_constant_medium,
            "camera",
            set_camera,
            "max_depth",
            &configuration::max_depth,
            "samples_per_pixel",
            &configuration::samples_per_pixel,
            "pixels_per_process",
            &configuration::pixels_per_process,
            "background_color",
            &configuration::background_color);
    }

    // register helpful shape functions
    lua["make_sphere"] = make_sphere_;
    lua["make_xy_rect"] = make_xy_rect_;
    lua["make_xz_rect"] = make_xz_rect_;
    lua["make_yz_rect"] = make_yz_rect_;
    lua["make_box"] = make_box_;
    lua["make_moving_sphere"] = make_moving_sphere_;
    lua["make_translate_shape"] = make_translate_shape_;
    lua["make_rotate_shape"] = make_rotate_shape_;
    lua["make_constant_medium"] = make_constant_medium_;

    auto result = lua.safe_script_file(input_file.data(), sol::script_pass_on_error);

    if (!result.valid()) {
        sol::error err = result;
        fmt::print(stderr,
                   "error encountered processing '{}'\n>>>\n{}\n<<<\n",
                   input_file,
                   err.what());
        return std::nullopt;
    }
    return result;
}

std::optional<configuration> input(std::string_view input_file)
{
    std::optional<configuration> conf{std::nullopt};

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int success_flag = 1;
    int fail_flag = 0;

    if (rank == 0) {
        conf = input_(input_file);
        // Let other procs know if conf has failed
        if (!conf) {
            MPI_Bcast(&fail_flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
            return conf;
        } else {
            MPI_Bcast(&success_flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
        }

        // serialize configuration data for mpi
        bytes::archiver ar{};
        bytes::to_bytes(ar, *conf);

        // communicate size first so receiving procs can allocate appropriate mem
        int sz = ar.size();
        MPI_Bcast(&sz, 1, MPI_INT, 0, MPI_COMM_WORLD);
        MPI_Bcast(ar.data(), sz, MPI_BYTE, 0, MPI_COMM_WORLD);

    } else {
        int flag;
        MPI_Bcast(&flag, 1, MPI_INT, 0, MPI_COMM_WORLD);
        if (flag == fail_flag) return conf;

        int sz;
        MPI_Bcast(&sz, 1, MPI_INT, 0, MPI_COMM_WORLD);

        bytes::archiver ar{sz};
        MPI_Bcast(ar.data(), sz, MPI_BYTE, 0, MPI_COMM_WORLD);

        conf = bytes::to_obj<configuration>(ar);
    }

    return conf;
}

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>

using vec3 = glm::dvec3;
using vec4 = glm::dvec4;
using color3 = vec3;
using point3 = vec3;
using point4 = vec4;
using vec2 = glm::dvec2;
using point2 = vec2;

using mat4 = glm::dmat4;
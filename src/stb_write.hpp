#pragma once

#include <array>
#include <glm/common.hpp>
#include <glm/exponential.hpp>
#include "floating_types.hpp"
#include "vector_types.hpp"
#include <memory>
#include <span>
#include <string_view>
#include <vector>

#include <mpi.h>
#include <utility>

using pixel_t = std::array<unsigned char, 3>;

class png_pixel
{
    int offset;
    bool write;
    std::vector<pixel_t>& buffered_pixels;
    MPI_Win w;

public:
    png_pixel(int offset, bool write, std::vector<pixel_t>& buf, MPI_Win w)
        : offset{offset}, write{write}, buffered_pixels{buf}, w{w}
    {
    }
    png_pixel& operator=(const color3& v);
};

// wrapper class for interacting with stb_image_write for pngs
class stb_png_writer
{
    int nx;
    int ny;
    int pixels_per_process;
    std::vector<pixel_t> buffered_pixels;
    int rank;
    std::span<pixel_t> image;
    MPI_Win w;

public:
    // construct an image of nx x ny dimensions in an rgb space
    stb_png_writer(int nx, int ny, int pixels_per_process);

    // write data to filename
    void write(std::string_view filename) const;

    png_pixel pixel(int i, int j);

    void print_pixel(int i, int j) const;

    //    ~stb_png_writer();
};

#pragma once

#include "bytes.hpp"

#include <optional>
#include <variant>

#include "core/hit.hpp"

#include "textures/texture.hpp"

// forward decls for materials
struct lambertian;
struct dielectric;
struct metal;
struct diffuse_light;
struct isotropic;

namespace tags
{

template <typename T>
struct material_t {
};

template <typename... T>
using material_tag_ = std::variant<material_t<T>...>;

} // namespace tags

// material tags written by serializer
using material_tag =
    tags::material_tag_<lambertian, dielectric, metal, diffuse_light, isotropic>;

// Concept for materials - Note that the CPO check requires a complete type so we can't
// put it here.
// A Material also has an optional `emitted` method
template <typename T>
concept Material = requires(const T& mat, const ray& r, const hit_record& hit)
{
    {
        mat.scatter(r, hit)
    }
    ->std::same_as<std::optional<scatter_record>>;
};

class material
{
    // private nested classes for type erasure
    class any_material
    {
    public:
        virtual ~any_material() {}
        virtual any_material* clone() const = 0;
        virtual std::optional<scatter_record> scatter(const ray&,
                                                      const hit_record&) const = 0;
        virtual void to_bytes(bytes::archiver&) const = 0;
        virtual color3 emitted(real u, real v, const point3& p) const = 0;
    };

    template <Material S>
    class any_material_impl : public any_material
    {
        S s;

    public:
        any_material_impl(const S& s) : s{s} {}
        any_material_impl(S&& s) : s{std::move(s)} {}

        any_material_impl* clone() const override { return new any_material_impl(s); }

        std::optional<scatter_record> scatter(const ray& r,
                                              const hit_record& h) const override
        {
            return s.scatter(r, h);
        }

        color3 emitted(real u, real v, const point3& p) const override
        {
            // provide default impl for shapes 's' that do not have a emitted method
            constexpr bool has_emitted =
                requires(const S& s, real u, real v, const point3& p)
            {
                {
                    s.emitted(u, v, p)
                }
                ->std::same_as<color3>;
            };
            if constexpr (has_emitted)
                return s.emitted(u, v, p);
            else
                return {0, 0, 0};
        }

        void to_bytes(bytes::archiver& ar) const override
        {
            material_tag tag{tags::material_t<S>{}};
            bytes::to_bytes(ar, tag);
            return bytes::to_bytes(ar, s);
        }
    };

    any_material* s;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const material& t);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, material& t);

public:
    material() : s{nullptr} {}

    material(const material& other) : s{nullptr}
    {
        if (other) s = other.s->clone();
    }

    material(material&& other) : s{std::exchange(other.s, nullptr)} {}

    // construction from anything with a hit method
    template <Material S>
    requires(!std::same_as<material, std::remove_cvref_t<S>>) material(S&& other)
        : s{new any_material_impl{std::forward<S>(other)}}
    {
    }

    material& operator=(const material& other)
    {
        material tmp{other};
        swap(*this, tmp);
        return (*this);
    }

    material& operator=(material&& other)
    {
        delete s;
        s = std::exchange(other.s, nullptr);
        return *this;
    }

    ~material() { delete s; }

    friend void swap(material& x, material& y) { std::swap(x.s, y.s); }

    explicit operator bool() const { return s != nullptr; }

    std::optional<scatter_record> scatter(const ray& r, const hit_record& h) const
    {
        return s->scatter(r, h);
    }

    color3 emitted(real u, real v, const point3& p) const { return s->emitted(u, v, p); }
};

material make_lambertian(const texture&);

material make_metal(const color3&, real fuzz);

material make_dielectric(real index_of_refraction);

material make_diffuse_light(const color3&);

material make_isotropic(const texture&);
#include "lambertian.hpp"
#include "material.hpp"

#include <glm/gtc/random.hpp>

#include "core/hit.hpp"
#include "core/ray.hpp"

#include "textures/textures.hpp"

std::optional<scatter_record> lambertian::scatter(const ray& ray_in,
                                                  const hit_record& hit) const
{
    // lambertian material simply scatters a ray in random direction
    return scatter_record{albedo.value(hit.u, hit.v, hit.point),
                          ray{.origin = hit.point,
                              .direction = hit.normal + glm::sphericalRand(real(1)),
                              .time = ray_in.time}};
}

material make_lambertian(const texture& t) { return {lambertian{t}}; }

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const lambertian& s)
{
    bytes::to_bytes(ar, s.albedo);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, lambertian& s)
{
    s.albedo = bytes::to_obj<texture>(ar);
}

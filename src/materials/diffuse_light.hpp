#pragma once

#include "core/fwd.hpp"
#include <optional>

#include "textures/texture.hpp"

struct diffuse_light {
    texture emit;

    std::optional<scatter_record> scatter(const ray&, const hit_record&) const;

    color3 emitted(real u, real v, const point3& p) const;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver&,
                           const diffuse_light&);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, diffuse_light&);
};

#include "diffuse_light.hpp"
#include "core/hit.hpp"
#include "core/ray.hpp"
#include "material.hpp"

std::optional<scatter_record> diffuse_light::scatter(const ray& r,
                                                    const hit_record& hit) const
{
    return std::nullopt;
}

color3 diffuse_light::emitted(real u, real v, const point3& p) const
{
    return emit.value(u, v, p);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const diffuse_light& m)
{
    bytes::to_bytes(ar, m.emit);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, diffuse_light& m)
{
    m.emit = bytes::to_obj<texture>(ar);
}

material make_diffuse_light(const color3& color)
{
    return {diffuse_light{make_solid_color(color)}};
}

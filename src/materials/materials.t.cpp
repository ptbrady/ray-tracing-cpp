#include "materials.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Lambertian")
{
    auto t = make_checkered(make_solid_color(vec3{0.1, 0.2, 0.3}),
                            make_solid_color(vec3{0.1, 0.2, 0.3}));
    auto l = make_lambertian(t);

    REQUIRE(l);

    bytes::archiver ar{};
    bytes::to_bytes(ar, l);
    auto ll = bytes::to_obj<material>(ar);    

    REQUIRE(ll);

    auto s = ll.scatter(ray{}, hit_record{ray{}, 0, 0, 0, vec3{}, vec3{}, l});
    REQUIRE(s);
    REQUIRE(s->attenuation == vec3{0.1, 0.2, 0.3});
}

TEST_CASE("Dielectic")
{
    auto mat = make_dielectric(0.5);
    REQUIRE(mat);

    bytes::archiver ar{};
    bytes::to_bytes(ar, mat);
    auto m = bytes::to_obj<material>(ar);

    REQUIRE(m);
}

TEST_CASE("Metal")
{
    auto mat = make_metal(vec3{0, 0, 0}, 0.5);
    REQUIRE(mat);

    bytes::archiver ar{};
    bytes::to_bytes(ar, mat);
    auto m = bytes::to_obj<material>(ar);

    REQUIRE(m);
}

TEST_CASE("Diffuse Light")
{
    auto color = vec3{0.1, 0.2, 0.3};
    auto mat = make_diffuse_light(color);
    REQUIRE(mat);

    REQUIRE(mat.emitted(0, 0, vec3{}) == color);

    bytes::archiver ar{};
    bytes::to_bytes(ar, mat);

    auto m = bytes::to_obj<material>(ar);

    REQUIRE(m.emitted(0, 0, vec3{}) == color);
}

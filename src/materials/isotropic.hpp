#pragma once

#include "core/fwd.hpp"
#include <optional>

#include "textures/texture.hpp"

struct isotropic {
    texture albedo;

    std::optional<scatter_record> scatter(const ray&, const hit_record&) const;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver&,
                           const isotropic&);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, isotropic&);
};
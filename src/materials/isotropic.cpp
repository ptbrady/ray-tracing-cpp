#include "isotropic.hpp"
#include "textures/textures.hpp"
#include <glm/gtc/random.hpp>
#include "core/hit.hpp"
#include "material.hpp"

std::optional<scatter_record> isotropic::scatter(const ray& r, const hit_record& h) const
{
    return scatter_record{.attenuation = albedo.value(h.u, h.v, h.point),
                          .scattered = ray{h.point, glm::ballRand(real(1)), r.time}};
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const isotropic& m)
{
    bytes::to_bytes(ar, m.albedo);
}

void
tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, isotropic& m)
{
    m.albedo = bytes::to_obj<texture>(ar);
}

material make_isotropic(const texture& albedo) { return {isotropic{albedo}}; }
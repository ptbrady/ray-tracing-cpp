#pragma once
#include "core/fwd.hpp"
#include "floating_types.hpp"

#include <optional>

struct dielectric {
    real eta; // index of refraction

    std::optional<scatter_record> scatter(const ray& ray_in, const hit_record& hit) const;
};

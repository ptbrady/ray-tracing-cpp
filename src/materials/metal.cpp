#include "metal.hpp"
#include <glm/geometric.hpp>
#include <glm/gtc/random.hpp>

#include "core/hit.hpp"
#include "core/ray.hpp"
#include "material.hpp"

std::optional<scatter_record> metal::scatter(const ray& ray_in,
                                             const hit_record& hit) const
{
    // use fuzz * glm::ballRand(1) rather than glm::ballRand(fuzz) since the latter
    // errors out if fuzz == 0
    auto reflected_direction =
        glm::reflect(glm::normalize(ray_in.direction), hit.normal) +
        fuzz * glm::ballRand(real(1.0));
    if (glm::dot(reflected_direction, hit.normal) > 0)
        return scatter_record{albedo,
                              ray{.origin = hit.point,
                                  .direction = reflected_direction,
                                  .time = ray_in.time}};
    else
        return std::nullopt;
}

material make_metal(const color3& albedo, real fuzz) { return {metal{albedo, fuzz}}; }

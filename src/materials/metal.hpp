#pragma once

#include "core/fwd.hpp"

#include "floating_types.hpp"
#include "vector_types.hpp"
#include <optional>

struct metal {
    color3 albedo;
    real fuzz;

    std::optional<scatter_record> scatter(const ray& ray_in, const hit_record& hit) const;
};

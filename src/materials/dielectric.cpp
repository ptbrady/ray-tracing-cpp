#include "dielectric.hpp"

#include <glm/geometric.hpp>
#include <glm/gtc/random.hpp>

#include "core/ray.hpp"
#include "core/hit.hpp"

#include "material.hpp"

static real schlick(real cosine, real ref_idx)
{
    auto r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 *= r0;
    return r0 + (1 - r0) * std::pow(1 - cosine, 5);
}

std::optional<scatter_record> dielectric::scatter(const ray& ray_in,
                                                  const hit_record& hit) const
{
    // dielectric which refracts whenever possible
    real eta_ratio = hit.ray_outside ? 1 / eta : eta;
    auto in_direction = glm::normalize(ray_in.direction);
    real cos_theta = std::min(glm::dot(-in_direction, hit.normal), real(1));
    real sin_theta = std::sqrt(1 - cos_theta * cos_theta);

    // check for internal reflection or shclick based reflection
    if (eta_ratio * sin_theta > 1 ||
        glm::linearRand(real(0.0), real(1.0)) < schlick(cos_theta, eta_ratio))
        return scatter_record{.attenuation = vec3{1, 1, 1},
                              .scattered =
                                  ray{.origin = hit.point,
                                      .direction = glm::reflect(in_direction, hit.normal),
                                      .time = ray_in.time}};
    else
        return scatter_record{.attenuation = vec3{1, 1, 1},
                              .scattered = ray{.origin = hit.point,
                                               .direction = glm::refract(
                                                   in_direction, hit.normal, eta_ratio),
                                               .time = ray_in.time}};
}

material make_dielectric(real index_of_refraction) { return {dielectric{index_of_refraction}}; }

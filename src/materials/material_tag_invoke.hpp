#pragma once

#include "material.hpp"

inline void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const material& m)
{
        m.s->to_bytes(ar);
}

inline void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, material& t)
{
        auto tag = bytes::to_obj<material_tag>(ar);
        std::visit(
            [&ar, &t]<typename T>(tags::material_t<T>) { t = bytes::to_obj<T>(ar); },
            tag);
}

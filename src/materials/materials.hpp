#pragma once

#include "material.hpp"

#include "lambertian.hpp"
#include "dielectric.hpp"
#include "metal.hpp"
#include "diffuse_light.hpp"
#include "isotropic.hpp"

#include "material_tag_invoke.hpp"

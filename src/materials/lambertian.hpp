#pragma once

#include <optional>

#include "core/fwd.hpp"
#include "textures/texture.hpp"

struct lambertian {
    texture albedo;

    std::optional<scatter_record> scatter(const ray& ray_in, const hit_record& hit) const;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const lambertian& s);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, lambertian& s);
};

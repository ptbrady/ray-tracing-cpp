#include "checkered.hpp"
#include "image_tex.hpp"
#include "noise.hpp"
#include "solid_color.hpp"
#include "texture.hpp"

#include <stdexcept>

// stb_image
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const stb_image& s)
{
    bytes::to_bytes(ar, s.width);
    bytes::to_bytes(ar, s.height);
    bytes::to_bytes(ar, s.image);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, stb_image& s)
{
    s.width = bytes::to_obj<int>(ar);
    s.height = bytes::to_obj<int>(ar);
    s.image = bytes::to_obj<decltype(s.image)>(ar);
}

// image_tex
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const image_tex& i)
{
    bytes::to_bytes(ar, i.image_);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, image_tex& i)
{
    i.image_ = bytes::to_obj<stb_image>(ar);
}

// noise
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const noise& t)
{
    bytes::to_bytes(ar, t.perlin_);
    bytes::to_bytes(ar, t.scale);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, noise& t)
{
    t.perlin_ = bytes::to_obj<perlin>(ar);
    t.scale = bytes::to_obj<real>(ar);
}

// checkered
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const checkered& t)
{
    bytes::to_bytes(ar, t.odd);
    bytes::to_bytes(ar, t.even);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, checkered& t)
{
    t.odd = bytes::to_obj<texture>(ar);
    t.even = bytes::to_obj<texture>(ar);
}

// texture
void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const texture& t)
{
    t.s->to_bytes(ar);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, texture& t)
{
    auto tag = bytes::to_obj<texture_tag>(ar);

    std::visit([&ar, &t]<typename T>(tags::texture_t<T>) { t = bytes::to_obj<T>(ar); },
              tag);
}

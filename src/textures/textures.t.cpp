#include "textures.hpp"

#include "random.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("solid_color")
{
    const auto color = vec3{0.1, 0.2, 0.3};
    texture t = make_solid_color(color);  

    REQUIRE(t.value(0, 0, vec3{-1, -1, 2}) == color);

    bytes::archiver ar{};

    bytes::to_bytes(ar, t);
    auto tt = bytes::to_obj<texture>(ar);

    REQUIRE(tt.value(0, 0, vec3{-1, -1, 2}) == color);
}

TEST_CASE("checkered")
{
    const auto color_odd = vec3(0.1, 0.2, 0.3);
    const auto color_even = color_odd;

    texture t = make_checkered(solid_color{color_odd}, solid_color{color_even});

    REQUIRE(t.value(0, 0, vec3{-1, -1, 2}) == color_odd);

    bytes::archiver ar{};

    bytes::to_bytes(ar, t);
    auto tt = bytes::to_obj<texture>(ar);

    REQUIRE(tt.value(0, 1, vec3{-1, -1, 2}) == color_odd);
}

TEST_CASE("noise")
{
    texture t = make_noise(256, 0.5);

    auto point = vec3{pick(), pick(), pick()};
    real u = pick();
    real v = pick();

    bytes::archiver ar{};
    bytes::to_bytes(ar, t);
    auto tt = bytes::to_obj<texture>(ar);

    REQUIRE(t.value(u, v, point) == tt.value(u, v, point));
}

TEST_CASE("image")
{
    texture t = make_image(nullptr);    
}

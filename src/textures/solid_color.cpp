#include "solid_color.hpp"
#include "texture.hpp"

texture make_solid_color(const color3& rgb) {
    return {solid_color{rgb}};
}

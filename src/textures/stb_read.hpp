#pragma once
#include "bytes.hpp"
#include "floating_types.hpp"

#include <array>
#include <vector>

constexpr auto bytes_per_pixel = 3;
using read_pixel_t = std::array<unsigned char, bytes_per_pixel>;

struct stb_image {
    int width;
    int height;
    std::vector<read_pixel_t> image;

    stb_image() = default;

    stb_image(const char* filename);

    const read_pixel_t& operator()(int i, int j) const;
    read_pixel_t& operator()(int i, int j);

    // texture coordinates in [0,1]
    const read_pixel_t& operator()(real u, real v) const;
    read_pixel_t& operator()(real u, real v);

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const stb_image& s);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, stb_image& s);
};

#pragma once

#include "perlin.hpp"

struct noise {
    perlin perlin_;
    real scale;

    noise() = default;
    noise(int n, real scale) : perlin_{n}, scale{scale} {}

    color3 value(real u, real v, const point3& p) const;

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const noise& t);
    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, noise& t);
};

#pragma once

#include "floating_types.hpp"
#include "vector_types.hpp"

struct solid_color {
    color3 rgb;

    color3 value(real, real, const point3&) const { return rgb; }
};

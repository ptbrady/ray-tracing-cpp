#include "image_tex.hpp"
#include <algorithm>
#include "texture.hpp"


image_tex::image_tex(const char* filename) : image_{filename} {}

color3 image_tex::value(real u, real v, const point3&) const
{
    // ensure texture coordinates [0, 1]
    u = std::clamp(u, real(0), real(1));
    v = 1 - std::clamp(v, real(0), real(1)); // flip to image coords

    auto pixel = image_(u, v);

    constexpr real color_scale = real(1) / 255;

    return color3{
        pixel[0] * color_scale, pixel[1] * color_scale, pixel[2] * color_scale};
}

texture make_image(const char* filename) { return image_tex{filename}; }

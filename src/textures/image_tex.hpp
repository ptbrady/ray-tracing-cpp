#pragma once

#include "bytes.hpp"
#include "stb_read.hpp"
#include "vector_types.hpp"

struct image_tex {
    stb_image image_;

    image_tex() = default;

    image_tex(const char* filename);

    color3 value(real u, real v, const point3& p) const;
};

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver&, const image_tex&);

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, image_tex&);

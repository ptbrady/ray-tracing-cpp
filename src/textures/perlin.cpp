#include "perlin.hpp"
#include "random.hpp"
#include <algorithm>
#include <glm/gtc/random.hpp>
#include <numeric>

static real perlin_interp(point3 c[2][2][2], real u, real v, real w)
{
    // hermite smmoothing
    u *= u * (3 - 2 * u);
    v *= v * (3 - 2 * v);
    w *= w * (3 - 2 * w);

    real acc = 0;
    for (int i = 0; i < 2; i++)
        for (int j = 0; j < 2; j++)
            for (int k = 0; k < 2; k++) {
                vec3 weight{u - i, v - j, w - k};

                acc += (i * u + (1 - i) * (1 - u)) * (j * v + (1 - j) * (1 - v)) *
                       (k * w + (1 - k) * (1 - w)) * glm::dot(c[i][j][k], weight);
            }

    return acc;
}

static void perlin_generate_perm(std::vector<int>& p)
{
    std::iota(p.begin(), p.end(), 0);

    // is there any reason we shouldn't just use std::shuffle here?
    std::shuffle(p.begin(), p.end(), global_urng());
    // for (int i = p.size() - 1; i > 0; i--)
    //    std::swap(p[i], p[pick(0, i)]);
}

perlin::perlin(int n) : rand(n), perm_x(n), perm_y(n), perm_z(n)
{
    for (auto&& v : rand) v = glm::sphericalRand(1.0);

    perlin_generate_perm(perm_x);
    perlin_generate_perm(perm_y);
    perlin_generate_perm(perm_z);
}

real perlin::noise(const point3& p) const
{
    int max = rand.size() - 1;

    auto u = p.x - std::floor(p.x);
    auto v = p.y - std::floor(p.y);
    auto w = p.z - std::floor(p.z);

    int i = std::floor(p.x);
    int j = std::floor(p.y);
    int k = std::floor(p.z);
    vec3 c[2][2][2];

    for (int di = 0; di < 2; di++)
        for (int dj = 0; dj < 2; dj++)
            for (int dk = 0; dk < 2; dk++)
                c[di][dj][dk] = rand[perm_x[(i + di) & max] ^ perm_y[(j + dj) & max] ^
                                     perm_z[(k + dk) & max]];

    return perlin_interp(c, u, v, w);
}

real perlin::turbulence(const point3& p, int depth) const 
{
    real acc = 0;
    auto point = p;
    real weight = 1;

    for (int i = 0; i < depth; i++) {
        acc += weight * noise(point);
        weight /= 2;
        point *= 2;
    }

    return std::abs(acc);
}

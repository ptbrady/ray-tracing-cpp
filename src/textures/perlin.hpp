#pragma once

#include "bytes.hpp"
#include <vector>
#include "floating_types.hpp"
#include "vector_types.hpp"

class perlin
{
    std::vector<vec3> rand;
    std::vector<int> perm_x;
    std::vector<int> perm_y;
    std::vector<int> perm_z;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const perlin& p)
    {
        bytes::to_bytes(ar, p.rand);
        bytes::to_bytes(ar, p.perm_x);
        bytes::to_bytes(ar, p.perm_y);
        bytes::to_bytes(ar, p.perm_z);
    }

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, perlin& p)
    {
        p.rand = bytes::to_obj<decltype(p.rand)>(ar);
        p.perm_x = bytes::to_obj<decltype(p.perm_x)>(ar);
        p.perm_y = bytes::to_obj<decltype(p.perm_y)>(ar);
        p.perm_z = bytes::to_obj<decltype(p.perm_z)>(ar);
    }

public:
    perlin() = default;
    perlin(int n);

    real noise(const point3& point) const;

    real turbulence(const point3& point, int depth = 7) const;
};

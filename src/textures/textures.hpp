#pragma once
#include "texture.hpp"

#include "solid_color.hpp"
#include "noise.hpp"
#include "stb_read.hpp"
#include "image_tex.hpp"
#include "checkered.hpp"

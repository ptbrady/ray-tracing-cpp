#pragma once

#include "bytes.hpp"

#include "floating_types.hpp"
#include "vector_types.hpp"
#include <variant>

// forward decls for textures
struct solid_color;
struct checkered;
struct noise;
struct image_tex;

namespace tags
{

template <typename T>
struct texture_t {
};

template <typename... T>
using texture_tag_ = std::variant<texture_t<T>...>;

} // namespace tags

// texture tags written by serializer
using texture_tag = tags::texture_tag_<solid_color, checkered, noise, image_tex>;

// Concept for textures - Note that the CPO check requires a complete type so we can't
// put it here.
template <typename T>
concept Texture = requires(const T& tex, real u, real v, const point3& p) {
    {tex.value(u, v, p)} -> std::same_as<color3>;
};

class texture {
// private nested classes for type erasure
    class any_texture
    {
    public:
        virtual ~any_texture() {}
        virtual any_texture* clone() const = 0;
        virtual color3 value(real, real, const point3&) const = 0;
        virtual void to_bytes(bytes::archiver&) const = 0;
    };

    template <Texture S>
    class any_texture_impl : public any_texture
    {
        S s;

    public:
        any_texture_impl(const S& s) : s{s} {}
        any_texture_impl(S&& s) : s{std::move(s)} {}

        any_texture_impl* clone() const override { return new any_texture_impl(s); }
        
        color3
        value(real u, real v, const point3& p) const override
        {
            return s.value(u, v, p);
        }    

        void to_bytes(bytes::archiver& ar) const override
        {
            texture_tag tag{tags::texture_t<S>{}};
            bytes::to_bytes(ar, tag);
            return bytes::to_bytes(ar, s);
        }
    };

    any_texture* s;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const texture& t);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, texture& t);

public:
    texture() : s{nullptr} {}

    texture(const texture& other) : s{nullptr}
    {
        if (other) s = other.s->clone();
    }

    texture(texture&& other) : s{std::exchange(other.s, nullptr)} {}

    // construction from anything with a hit method
    template <Texture S>
    requires (!std::same_as<texture, std::remove_cvref_t<S>>)
    texture(S&& other) : s{new any_texture_impl{std::forward<S>(other)}}
    {
    }

    texture& operator=(const texture& other)
    {
        texture tmp{other};
        swap(*this, tmp);
        return (*this);
    }

    texture& operator=(texture&& other)
    {
        delete s;
        s = std::exchange(other.s, nullptr);
        return *this;
    }

    ~texture() { delete s; }

    friend void swap(texture& x, texture& y) { std::swap(x.s, y.s); }

    explicit operator bool() const { return s != nullptr; }

    color3 value(real u, real v, const point3& p) const {
        return s->value(u, v, p);
    }   
};

texture make_solid_color(const vec3&);

texture make_checkered(const texture& even, const texture& odd);

texture make_noise(int n, real scale);

texture make_image(const char* filename);

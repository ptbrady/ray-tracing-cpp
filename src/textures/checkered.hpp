#pragma once

#include "texture.hpp"

struct checkered {
    texture even;
    texture odd;

    color3 value(real u, real v, const point3& p) const;
};

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const checkered& t);

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, checkered& t);

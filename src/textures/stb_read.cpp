#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"

#include "stb_read.hpp"
#include <cassert>
#include <stdexcept>

stb_image::stb_image(const char* filename)
{
    if (!filename) return;
    
    int channels_in_file;

    unsigned char* data =
        stbi_load(filename, &width, &height, &channels_in_file, bytes_per_pixel);
    if (!data) throw std::runtime_error("unable to open specified file");

    image.resize(width * height);
    std::memcpy(static_cast<void*>(&image[0]), data, width * height * bytes_per_pixel);
    free(data);
}

const read_pixel_t& stb_image::operator()(int i, int j) const
{
    assert(j * width + i < width * height);
    return image[j * width + i];
}

read_pixel_t& stb_image::operator()(int i, int j)
{
    assert(j * width + i < width * height);
    return image[j * width + i];
}

// texture coords
const read_pixel_t& stb_image::operator()(real u, real v) const
{
    int i = std::min(width - 1, static_cast<int>(u * width));
    int j = std::min(height - 1, static_cast<int>(v * height));
    return (*this)(i, j);
}

read_pixel_t& stb_image::operator()(real u, real v)
{
    int i = std::min(width - 1, static_cast<int>(u * width));
    int j = std::min(height - 1, static_cast<int>(v * height));
    return (*this)(i, j);
}


#include "perlin.hpp"
#include "random.hpp"

#include <catch2/catch_test_macros.hpp>

TEST_CASE("serializability")
{
    randomize();

    auto p = perlin{256};
    auto point = vec3{pick(), pick(), pick()};
    auto f =  p.noise(point);

    bytes::archiver ar{};

    bytes::to_bytes(ar, p);

    auto pp = bytes::to_obj<perlin>(ar);

    REQUIRE(f == pp.noise(point));
}

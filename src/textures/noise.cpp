#include "noise.hpp"
#include "texture.hpp"
#include <cmath>

color3 noise::value(real u, real v, const point3& p) const
{
    return vec3{1, 1, 1} * real(0.5) *
           (1 + std::sin(scale * p.z + 10 * perlin_.turbulence(p)));
}

texture make_noise(int n, real scale) { return noise(n, scale); }

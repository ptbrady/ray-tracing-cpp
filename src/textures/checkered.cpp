#include "checkered.hpp"

#include "textures.hpp"

#include <cmath>

color3 checkered::value(real u, real v, const point3& p) const
{
    auto s = std::sin(10 * p.x) * std::sin(10 * p.y) * std::sin(10 * p.z);
    if (s < 0)
        return odd.value(u, v, p);
    else
        return even.value(u, v, p);
}

texture make_checkered(const texture& even, const texture& odd)
{
    return {checkered(even, odd)};
}

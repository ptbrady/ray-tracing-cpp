#pragma once

#include <glm/vec3.hpp>

#include "core/ray.hpp"
#include <glm/gtc/random.hpp>
class camera
{
    point3 origin;
    point3 lower_left_corner;
    vec3 horizontal;
    vec3 vertical;
    vec3 u, v, w;
    real lens_radius;
    real time_min, time_max;

public:
    camera() = default;

    camera(const point3& look_from,
           const point3& look_at,
           const vec3& view_up,
           real vfov /* veritical field-of-view (radians) */,
           real aspect_ratio,
           real aperature,
           real focus_dist,
           real time_min,
           real time_max)
        : time_min{time_min}, time_max{time_max}
    {
        auto h = std::tan(vfov / 2);
        real viewport_height = 2 * h;
        real viewport_width = aspect_ratio * viewport_height;

        // u,v,w form basis vectors of camera space
        w = glm::normalize(look_from - look_at);
        u = glm::normalize(glm::cross(view_up, w));
        v = glm::cross(w, u);

        origin = look_from;
        horizontal = focus_dist * viewport_width * u;
        vertical = focus_dist * viewport_height * v;
        lower_left_corner =
            origin - horizontal / real(2) - vertical / real(2) - focus_dist * w;

        lens_radius = aperature / 2;
    }

    std::pair<real, real> time_range() const { return {time_min, time_max}; }

    ray ray_to(real s, real t) const
    {
        auto rd = lens_radius * glm::diskRand(real(1));
        auto offset = u * rd.x + v * rd.y;

        return {origin + offset,
                lower_left_corner + s * horizontal + t * vertical - origin - offset,
                glm::linearRand(time_min, time_max)};
    }
};

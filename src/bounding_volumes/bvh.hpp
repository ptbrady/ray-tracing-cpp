#pragma once

#include "floating_types.hpp"
#include "aabb.hpp"
#include "shapes/shape.hpp"
#include <concepts>
#include <vector>

class bvh
{
    shape root_node;

public:
    bvh(const std::vector<shape>& s, std::pair<real, real> time);

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;
};

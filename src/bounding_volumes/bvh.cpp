#include "bvh.hpp"
#include <algorithm>
#include <cassert>

#include "random.hpp"
#include <span>

struct bvh_node {
    aabb box;
    shape left, right;

    bvh_node() = default;

    bvh_node(shape&& left, real time_min, real time_max)
        : box{*left.bounding_box(time_min, time_max)}, left{std::move(left)}, right{}
    {
    }

    bvh_node(shape&& left, shape&& right, real time_min, real time_max)
        : box{*left.bounding_box(time_min, time_max) +
              *right.bounding_box(time_min, time_max)},
          left{std::move(left)},
          right{std::move(right)}
    {
    }

    void reset_box(real time_min, real time_max)
    {
        // left node is always set by construction
        box = *left.bounding_box(time_min, time_max);
        auto right_bb = right.bounding_box(time_min, time_max);

        if (right_bb) box += *right_bb;
    }

    std::optional<aabb> bounding_box(real, real) const { return box; }

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const
    {
        // check bounding box and exit early if not intersection
        if (!box.hit(r, t_min, t_max)) return std::nullopt;

        // need to adjust capture times when visiting right
        auto hit_left = left.hit(r, t_min, t_max);
        if (hit_left) t_max = hit_left->t;

        auto hit_right = right.hit(r, t_min, t_max);

        return hit_right ? hit_right : hit_left;
    }
};

static bvh_node build_hierarchy(std::span<shape> s, real time_min, real time_max)
{
    auto cmp = [axis = pick(0, 2), time_min, time_max](const shape& a, const shape& b) {
        aabb box_a = a.bounding_box(time_min, time_max).value();
        aabb box_b = b.bounding_box(time_min, time_max).value();
        return box_a.min[axis] < box_b.min[axis];
    };

    const auto n_objects = s.size();
    if (n_objects == 1) return bvh_node{std::move(s[0]), time_min, time_max};

    if (n_objects == 2) {
        if (cmp(s[0], s[1])) {
            return bvh_node{std::move(s[0]), std::move(s[1]), time_min, time_max};
        } else {
            return bvh_node{std::move(s[1]), std::move(s[0]), time_min, time_max};
        }
    }

    std::ranges::sort(s, cmp);

    return bvh_node{
        build_hierarchy(s.first(n_objects / 2), time_min, time_max),
        build_hierarchy(s.last(n_objects - (n_objects / 2)), time_min, time_max),
        time_min,
        time_max
    };
}

bvh::bvh(const std::vector<shape>& s, std::pair<real, real> time) : root_node{}
{
    // nodes.reserve(2 * shapes.size());
    // start our randomness
    randomize();
    auto [a, b] = time;
    std::vector<shape> shapes{s};
    root_node = build_hierarchy(shapes, a, b);

    // assert(nodes.size() <= 2 * shapes.size());
}

std::optional<hit_record> bvh::hit(const ray& r, real t_min, real t_max) const
{
    return root_node.hit(r, t_min, t_max);
}

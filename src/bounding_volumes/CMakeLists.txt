add_library(bvh bvh.cpp)
target_link_libraries(bvh PUBLIC glm::glm shapes PRIVATE random)
target_include_directories(bvh PUBLIC ..)

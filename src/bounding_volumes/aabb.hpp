#pragma once

#include "core/ray.hpp"
#include <utility>

#include <glm/common.hpp>

struct aabb {
    point3 min;
    point3 max;

    // note that this does not have the same interface as shape->hit
    // this is designed to provide a quick yes/no response
    bool hit(const ray& r, real t_min, real t_max) const
    {
        for (int i = 0; i < 3; i++) {
            auto d = 1.0f / r.direction[i];
            auto t0 = d * (min[i] - r.origin[i]);
            auto t1 = d * (max[i] - r.origin[i]);
            if (d < 0.0f) std::swap(t0, t1);

            t_min = t0 > t_min ? t0 : t_min;
            t_max = t1 < t_max ? t1 : t_max;
            if (t_max <= t_min) return false;
        }
        return true;
    }

    aabb& operator+=(const aabb& x)
    {
        this->min = glm::min(this->min, x.min);
        this->max = glm::max(this->max, x.max);
        return *this;
    }

    // add two boxes together to get a larger box
    friend aabb operator+(const aabb& x, const aabb& y)
    {
        auto z = x;
        z += y;
        return z;
    }
};

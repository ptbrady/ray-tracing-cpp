#include "box.hpp"
#include "bounding_volumes/aabb.hpp"
#include "shapes.hpp"


box::box(const point3& corner0, const point3& corner1, const material& mat)
    : c0{corner0}, c1{corner1}, sides{}
{
    sides.reserve(6);

    sides.push_back(make_xy_rect(vec2{c0}, vec2{c1}, c1.z, mat));
    sides.push_back(make_xy_rect(vec2{c0}, vec2{c1}, c0.z, mat));

    sides.push_back(
        make_xz_rect(vec2{c0.x, c0.z}, vec2{c1.x, c1.z}, c1.y, mat));
    sides.push_back(
        make_xz_rect(vec2{c0.x, c0.z}, vec2{c1.x, c1.z}, c0.y, mat));

    sides.push_back(
        make_yz_rect(vec2{c0.y, c0.z}, vec2{c1.y, c1.z}, c1.x, mat));
    sides.push_back(
        make_yz_rect(vec2{c0.y, c0.z}, vec2{c1.y, c1.z}, c0.x, mat));
}

std::optional<hit_record> box::hit(const ray& r, real t_min, real t_max) const
{
    std::optional<hit_record> box_hit{};
    // record hit on closest side
    for (auto&& s : sides) {
        if (auto side_hit = s.hit(r, t_min, t_max); side_hit) {
            box_hit = side_hit;
            t_max = side_hit->t;
        }
    }
    return box_hit;
}

std::optional<aabb> box::bounding_box(real, real) const { return aabb{c0, c1}; }

shape make_box(const point3& corner0, const point3& corner1, const material& mat)
{
    return {box{corner0, corner1, mat}};
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const box& bx) 
{
    bytes::to_bytes(ar, bx.c0);
    bytes::to_bytes(ar, bx.c1);
    bytes::to_bytes(ar, bx.sides);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, box& bx) 
{
    bx.c0 = bytes::to_obj<vec3>(ar);
    bx.c1 = bytes::to_obj<vec3>(ar);
    bx.sides = bytes::to_obj<std::vector<shape>>(ar);
}

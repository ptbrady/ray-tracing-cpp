#include "sphere.hpp"
#include "shape.hpp"
#include <glm/geometric.hpp>
#include <numbers>

#include "materials/materials.hpp"

#include "bounding_volumes/aabb.hpp"
#include "core/hit.hpp"
#include "core/ray.hpp"

std::optional<hit_record> sphere::hit(const ray& r, real t_min, real t_max) const
{
    vec3 oc = r.origin - center;
    auto a = glm::dot(r.direction, r.direction);
    auto b = glm::dot(oc, r.direction);
    auto c = glm::dot(oc, oc) - radius * radius;
    auto discriminant = b * b - a * c;

    if (discriminant > 0) {
        auto sqr = std::sqrt(discriminant);

        if (auto t = (-b - sqr) / a; t > t_min && t < t_max) {
            auto p = r.position(t);
            auto [u, v] = uv((p - center) / radius);
            return hit_record{r, t, u, v, p, (p - center) / radius, mat};
        }

        if (auto t = (-b + sqr) / a; t > t_min && t < t_max) {
            auto p = r.position(t);
            auto [u, v] = uv((p - center) / radius);
            return hit_record{r, t, u, v, p, (p - center) / radius, mat};
        }
    }
    return std::nullopt;
}

std::optional<aabb> sphere::bounding_box(real, real) const
{
    return aabb{center - radius, center + radius};
}

std::pair<real, real> sphere::uv(const point3& p)
{
    auto phi = std::atan2(p.z, p.x);
    auto theta = std::asin(p.y);
    real u = 1 - (phi + std::numbers::pi) / (2 * std::numbers::pi);
    real v = (theta + std::numbers::pi / 2) / std::numbers::pi;

    return {u, v};
}

shape make_sphere(const point3& center, real radius, const material& mat)
{
    return {sphere{center, radius, mat}};
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const sphere& s)
{
    bytes::to_bytes(ar, s.center);
    bytes::to_bytes(ar, s.radius);
    bytes::to_bytes(ar, s.mat);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, sphere& s)
{
    s.center = bytes::to_obj<point3>(ar);
    s.radius = bytes::to_obj<real>(ar);
    s.mat = bytes::to_obj<material>(ar);
}

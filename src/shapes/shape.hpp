#pragma once

#include "bytes.hpp"
#include <optional>
#include <stdexcept>
#include <variant>
#include <vector>

#include "bounding_volumes/aabb.hpp"
#include "core/hit.hpp"
#include "materials/material.hpp"

// some forward shape decls - add more for more shapes
struct sphere;
struct moving_sphere;
struct xz_rect;
struct yz_rect;
struct xy_rect;
struct box;
struct translate;
struct rotate;
struct constant_medium;

namespace tags
{
template <typename T>
struct shape_t {
};

template <typename... T>
using shape_tag_ = std::variant<shape_t<T>...>;
} // namespace tags
// Add shapes to shape_tag as they get added.
using shape_tag = tags::shape_tag_<sphere,
                                   moving_sphere,
                                   xy_rect,
                                   xz_rect,
                                   yz_rect,
                                   box,
                                   translate,
                                   rotate,
                                   constant_medium>;

// shape concept
template <typename S>
concept Shape = requires(const S& shape, const ray& r, real t)
{
    {
        shape.hit(r, t, t)
    }
    ->std::same_as<std::optional<hit_record>>;
    {
        shape.bounding_box(t, t)
    }
    ->std::same_as<std::optional<aabb>>;
};

// use type-erasure for defining shapes so we can more easily
// interact with lua and keep value semantics
class shape
{
    // private nested classes for type erasure
    class any_shape
    {
    public:
        virtual ~any_shape() {}
        virtual any_shape* clone() const = 0;
        virtual std::optional<hit_record> hit(const ray&, real, real) const = 0;
        virtual std::optional<aabb> bounding_box(real, real) const = 0;
        virtual void to_bytes(bytes::archiver&) const = 0;
    };

    template <Shape S>
    class any_shape_impl : public any_shape
    {
        S s;

    public:
        any_shape_impl(const S& s) : s{s} {}
        any_shape_impl(S&& s) : s{std::move(s)} {}

        any_shape_impl* clone() const override { return new any_shape_impl(s); }

        std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const override
        {
            return s.hit(r, t_min, t_max);
        }

        std::optional<aabb> bounding_box(real time_min, real time_max) const override
        {
            return s.bounding_box(time_min, time_max);
        }

        void to_bytes(bytes::archiver& ar) const override
        {
            // need to if constexpr this away if shape_t is not defined
            constexpr bool has_tag = requires(S s) { {shape_tag{tags::shape_t<S>{}}}; };
            if constexpr (has_tag) {
                shape_tag tag{tags::shape_t<S>{}};
                bytes::to_bytes(ar, tag);
                return bytes::to_bytes(ar, s);
            } else {
                throw std::runtime_error("to_bytes called on shape without a tag");
            }
        }
    };

    any_shape* s;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const shape& shp);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, shape& shp);

public:
    shape() : s{nullptr} {}

    shape(const shape& other) : s{nullptr}
    {
        if (other) s = other.s->clone();
    }

    shape(shape&& other) : s{std::exchange(other.s, nullptr)} {}

    // construction from anything with a hit method
    template <typename T>
        requires Shape<T> &&
        (!std::same_as<shape, std::remove_cvref_t<T>>)shape(T&& other)
        : s{new any_shape_impl{std::forward<T>(other)}}
    {
    }

    shape& operator=(const shape& other)
    {
        shape tmp{other};
        swap(*this, tmp);
        return (*this);
    }

    shape& operator=(shape&& other)
    {
        delete s;
        s = std::exchange(other.s, nullptr);
        return *this;
    }

    ~shape() { delete s; }

    friend void swap(shape& x, shape& y) { std::swap(x.s, y.s); }

    explicit operator bool() const { return s != nullptr; }

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const
    {
        if (*this)
            return s->hit(r, t_min, t_max);
        else
            return std::nullopt;
    }

    std::optional<aabb> bounding_box(real time_min = 0, real time_max = 0) const
    {
        if (*this)
            return s->bounding_box(time_min, time_max);
        else
            return std::nullopt;
    }
};

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const std::vector<shape>&);

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>,
                bytes::archiver& ar,
                std::vector<shape>&);

shape make_sphere(const point3& center, real radius, const material& mat);

shape make_moving_sphere(const point3& center_a,
                         const point3& center_b,
                         real time_a,
                         real time_b,
                         real radius,
                         const material& mat);

shape make_xy_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat);

shape make_xz_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat);
shape make_yz_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat);

shape make_box(const point3& corner0, const point3& corner1, const material& mat);

shape translate_shape(const shape&, const vec3& displacement);

shape rotate_y_shape(const shape&, real angle);
shape rotate_x_shape(const shape&, real angle);
shape rotate_z_shape(const shape&, real angle);

shape make_constant_medium(const shape&, const texture&, real density);

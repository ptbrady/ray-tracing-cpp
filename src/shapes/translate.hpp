#pragma once

#include "bounding_volumes/fwd.hpp"
#include "core/fwd.hpp"

#include "materials/material.hpp"
#include "shape.hpp"


struct translate {
    shape shape_;
    vec3 offset;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real time_min, real time_max) const;

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver&, const translate&);

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, translate&);
};

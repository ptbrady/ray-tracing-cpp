#pragma once

#include "bytes.hpp"

#include "core/fwd.hpp"

#include "bounding_volumes/fwd.hpp"
#include "materials/material.hpp"


struct sphere {
    point3 center;
    real radius;
    material mat;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real, real) const;

    static std::pair<real, real> uv(const point3& p);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const sphere& s);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, sphere& s);
};

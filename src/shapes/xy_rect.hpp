#pragma once

#include "rect.hpp"

extern template struct rect<2>;

struct xy_rect : rect<2> {};

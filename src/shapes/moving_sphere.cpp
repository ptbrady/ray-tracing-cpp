#include "moving_sphere.hpp"
#include "shape.hpp"

#include <glm/gtx/compatibility.hpp>

#include "bounding_volumes/aabb.hpp"
#include "core/hit.hpp"
#include "core/ray.hpp"

#include "materials/materials.hpp"

point3 moving_sphere::center_at(real t) const
{
    return glm::lerp(center_a, center_b, (t - time_a) / (time_b - time_a));
}

std::optional<hit_record> moving_sphere::hit(const ray& r, real t_min, real t_max) const
{
    point3 center = center_at(r.time);

    vec3 oc = r.origin - center;
    auto a = glm::dot(r.direction, r.direction);
    auto b = glm::dot(oc, r.direction);
    auto c = glm::dot(oc, oc) - radius * radius;
    auto discriminant = b * b - a * c;

    if (discriminant > 0) {
        auto sqr = std::sqrt(discriminant);

        if (auto t = (-b - sqr) / a; t > t_min && t < t_max) {
            auto p = r.position(t);
            auto [u, v] = sphere::uv((p - center) / radius);
            return hit_record{r, t, u, v, p, (p - center) / radius, mat};
        }

        if (auto t = (-b + sqr) / a; t > t_min && t < t_max) {
            auto p = r.position(t);
            auto [u, v] = sphere::uv((p - center) / radius);
            return hit_record{r, t, u, v, p, (p - center) / radius, mat};
        }
    }
    return std::nullopt;
}

std::optional<aabb> moving_sphere::bounding_box(real time_min, real time_max) const
{
    auto c_min = center_at(time_min);
    auto c_max = center_at(time_max);

    aabb box_min{c_min - radius, c_min + radius};
    aabb box_max{c_max - radius, c_max + radius};

    return box_min + box_max;
}

shape make_moving_sphere(const point3& center_a,
                         const point3& center_b,
                         real time_a,
                         real time_b,
                         real radius,
                         const material& mat)
{
    return {moving_sphere{center_a, center_b, time_a, time_b, radius, mat}};
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const moving_sphere& s)
{
    bytes::to_bytes(ar, s.center_a);
    bytes::to_bytes(ar, s.center_b);
    bytes::to_bytes(ar, s.time_a);
    bytes::to_bytes(ar, s.time_b);
    bytes::to_bytes(ar, s.radius);
    bytes::to_bytes(ar, s.mat);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, moving_sphere& s)
{
    s.center_a = bytes::to_obj<point3>(ar);
    s.center_b = bytes::to_obj<point3>(ar);
    s.time_a = bytes::to_obj<real>(ar);
    s.time_b = bytes::to_obj<real>(ar);
    s.radius = bytes::to_obj<real>(ar);
    s.mat = bytes::to_obj<material>(ar);
}

#pragma once

#include "bounding_volumes/aabb.hpp"
#include "core/fwd.hpp"

#include "materials/material.hpp"
#include "shape.hpp"


struct rotate {
    shape shape_;
    mat4 rotation;
    mat4 inv_rotation;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real time_min, real time_max) const;

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver&, const rotate&);

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, rotate&);
};

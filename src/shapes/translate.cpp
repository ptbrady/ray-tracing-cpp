#include "translate.hpp"
#include "bounding_volumes/aabb.hpp"
#include "shapes.hpp"

std::optional<hit_record> translate::hit(const ray& r, real t_min, real t_max) const
{
    if (auto h = shape_.hit(r.translate(offset), t_min, t_max); h) {
        h->point += offset;
        return h;
    }
    return std::nullopt;
}

std::optional<aabb> translate::bounding_box(real time_min, real time_max) const
{
    if (auto bx = shape_.bounding_box(time_min, time_max); bx) {
        return aabb{bx->min + offset, bx->max + offset};
    }
    return std::nullopt;
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const translate& t)
{
    bytes::to_bytes(ar, t.shape_);
    bytes::to_bytes(ar, t.offset);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, translate& t)
{
    t.shape_ = bytes::to_obj<shape>(ar);
    t.offset = bytes::to_obj<vec3>(ar);
}

shape translate_shape(const shape& s, const vec3& displacement)
{
    return {translate{s, displacement}};
}
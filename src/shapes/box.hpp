#pragma once

#include "bounding_volumes/fwd.hpp"
#include "core/fwd.hpp"

#include "materials/material.hpp"
#include "shape.hpp"

#include <vector>

struct box {
    point3 c0;
    point3 c1;
    std::vector<shape> sides;

    box() = default;
    box(const point3& corner0, const point3& corner1, const material& mat);

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real time_min, real time_max) const;

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver&, const box&);

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, box&);
};

#pragma once

#include "shape.hpp"

inline void
tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const shape& shp)
{
    shp.s->to_bytes(ar);
}

inline void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, shape& shp)
{
    auto tag = bytes::to_obj<shape_tag>(ar);
    std::visit([&ar, &shp]<typename T>(tags::shape_t<T>) { shp = bytes::to_obj<T>(ar); },
               tag);
}

inline void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                       bytes::archiver& ar,
                       const std::vector<shape>& shapes)
{
    bytes::to_bytes(ar, shapes.size());
    for (auto&& s : shapes) bytes::to_bytes(ar, s);
}

inline void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>,
                       bytes::archiver& ar,
                       std::vector<shape>& shapes)
{
    using sz_t = std::vector<shape>::size_type;
    auto sz = bytes::to_obj<sz_t>(ar);
    shapes.reserve(sz);
    for (sz_t i = 0; i < sz; i++) { shapes.push_back(bytes::to_obj<shape>(ar)); }
}

#include "rotate.hpp"
#include "shapes.hpp"
#include <glm/gtx/transform.hpp>

static ray rotate_ray(const ray& r, const mat4& m)
{
    const point4 o{r.origin, 1};
    const vec4 d{r.direction, 0};
    return ray{point3{m * o}, vec3{m * d}, r.time};
}

static aabb rotate_box(const aabb& bx, const mat4& m)
{
    auto [low, high] = bx;

    point4 min = m * point4{low, 1};
    point4 max = m * point4{low, 1};

    min = glm::min(min, m * point4{low.x, low.y, high.z, 1});
    max = glm::max(max, m * point4{low.x, low.y, high.z, 1});

    min = glm::min(min, m * point4{low.x, high.y, low.z, 1});
    max = glm::max(max, m * point4{low.x, high.y, low.z, 1});

    min = glm::min(min, m * point4{low.x, high.y, high.z, 1});
    max = glm::max(max, m * point4{low.x, high.y, high.z, 1});

    min = glm::min(min, m * point4{high.x, low.y, low.z, 1});
    max = glm::max(max, m * point4{high.x, low.y, low.z, 1});

    min = glm::min(min, m * point4{high.x, low.y, high.z, 1});
    max = glm::max(max, m * point4{high.x, low.y, high.z, 1});

    min = glm::min(min, m * point4{high.x, high.y, low.z, 1});
    max = glm::max(max, m * point4{high.x, high.y, low.z, 1});

    min = glm::min(min, m * point4{high.x, high.y, high.z, 1});
    max = glm::max(max, m * point4{high.x, high.y, high.z, 1});

    return aabb{point3{min}, point3{max}};
}

std::optional<hit_record> rotate::hit(const ray& r, real t_min, real t_max) const
{
    const auto rr = rotate_ray(r, inv_rotation);

    auto h = shape_.hit(rr, t_min, t_max);
    if (!h) return std::nullopt;

    // transform back
    h->point = rotation * point4{h->point, 1};
    vec3 n{rotation * vec4{h->normal, 0}};

    h->ray_outside = glm::dot(rr.direction, n) < 0;
    h->normal = h->ray_outside ? n : -n;
    return h;
}

std::optional<aabb> rotate::bounding_box(real time_min, real time_max) const
{
    auto bx = shape_.bounding_box(time_min, time_max);
    if (bx) return rotate_box(*bx, rotation);
    return std::nullopt;
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const rotate& s)
{
    bytes::to_bytes(ar, s.shape_);
    bytes::to_bytes(ar, s.rotation);
    bytes::to_bytes(ar, s.inv_rotation);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, rotate& s)
{
    s.shape_ = bytes::to_obj<shape>(ar);
    s.rotation = bytes::to_obj<mat4>(ar);
    s.inv_rotation = bytes::to_obj<mat4>(ar);
}

shape rotate_y_shape(const shape& s, real angle)
{
    auto mat = glm::rotate(angle, vec3{0, 1, 0});
    return {rotate{s, mat, glm::transpose(mat)}};
}

shape rotate_x_shape(const shape& s, real angle)
{
    auto mat = glm::rotate(angle, vec3{1, 0, 0});
    return {rotate{s, mat, glm::transpose(mat)}};
}

shape rotate_z_shape(const shape& s, real angle)
{
    auto mat = glm::rotate(angle, vec3{0, 0, 1});
    return {rotate{s, mat, glm::transpose(mat)}};
}

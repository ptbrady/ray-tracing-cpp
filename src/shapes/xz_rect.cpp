#include "xz_rect.hpp"
#include "shape.hpp"

template struct rect<1>;

shape make_xz_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat)
{
    return {xz_rect{corner0, corner1, k, mat}};
}

#pragma once

#include "sphere.hpp"

struct moving_sphere {
    point3 center_a, center_b;
    real time_a, time_b;
    real radius;
    material mat;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real time_min, real time_max) const;

    point3 center_at(real time) const;

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                           bytes::archiver& ar,
                           const moving_sphere& s);

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, moving_sphere& s);
};


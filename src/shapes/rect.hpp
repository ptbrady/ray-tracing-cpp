#pragma once

#include "bytes.hpp"
#include "core/hit.hpp"

#include "bounding_volumes/aabb.hpp"
#include "materials/materials.hpp"


namespace detail
{
template <int I>
struct indices;

// rect_yz
template <>
struct indices<0> {
    inline static constexpr std::array<int, 2> v{1, 2};
};
// rect_xz
template <>
struct indices<1> {
    inline static constexpr std::array<int, 2> v{0, 2};
};
// rect_xy
template <>
struct indices<2> {
    inline static constexpr std::array<int, 2> v{0, 1};
};
} // namespace detail

template <int I>
struct rect {
    // corner positions
    point2 c0;
    point2 c1;

    real plane_coord;
    material mat;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const
    {
        auto t = (plane_coord - r.origin[I]) / (r.direction[I]);

        if (t < t_min || t > t_max) return std::nullopt;

        const point3 p = r.position(t);
        auto [i, j] = detail::indices<I>::v;

        if (p[i] < c0[0] || p[i] > c1[0] || p[j] < c0[1] || p[j] > c1[1])
            return std::nullopt;

        auto uv = (vec2{p[i], p[j]} - c0) / (c1 - c0);

        vec3 normal {0, 0, 0};
        normal[I] = 1;

        return hit_record{r, t, uv[0], uv[1], p, normal, mat};
    }

    std::optional<aabb> bounding_box(real, real) const {
        auto [i,j] = detail::indices<I>::v;
        point3 min{};
        point3 max{};
        min[i] = c0[0]; max[i] = c1[0];
        min[j] = c0[1]; max[j] = c1[1];
        min[I] = plane_coord - real(0.0001);
        max[I] = plane_coord + real(0.0001);
        return aabb{min, max};
    }

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver& ar, const rect& s) {
        bytes::to_bytes(ar, s.c0);
        bytes::to_bytes(ar, s.c1);
        bytes::to_bytes(ar, s.plane_coord);
        bytes::to_bytes(ar, s.mat);
    }

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, rect& s) {
        s.c0 = bytes::to_obj<point2>(ar);
        s.c1 = bytes::to_obj<point2>(ar);
        s.plane_coord = bytes::to_obj<real>(ar);
        s.mat = bytes::to_obj<material>(ar);
    }
};

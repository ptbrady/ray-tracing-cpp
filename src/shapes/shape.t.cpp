#include "shapes.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_approx.hpp>

#include <numbers>

TEST_CASE("sphere")
{
    auto s =
        make_sphere(vec3{1, 1, 1}, 1, make_metal(vec3{}, 0));
    REQUIRE(s);

    bytes::archiver ar{};
    bytes::to_bytes(ar, s);

    auto ss = bytes::to_obj<shape>(ar);

    auto box = ss.bounding_box();

    REQUIRE(box);

    REQUIRE(box->min == vec3{0, 0, 0});
    REQUIRE(box->max == vec3{2, 2, 2});
}

TEST_CASE("moving sphere")
{
    auto color = make_solid_color(vec3(0.1, 0.2, 0.3));
    auto t = make_checkered(color, color);
    auto center_a = vec3{0, 0, 0};
    auto center_b = vec3(2, 2, 2);
    real time_a = 0;
    real time_b = 2;
    real radius = 1;
    auto s = make_moving_sphere(
        center_a, center_b, time_a, time_b, radius, make_lambertian(t));

    REQUIRE(s);

    bytes::archiver ar{};
    bytes::to_bytes(ar, s);

    auto ss = bytes::to_obj<shape>(ar);
    REQUIRE(ss);

    auto box = ss.bounding_box(-1, 1);
    REQUIRE(box);

    REQUIRE(box->min == vec3{-2, -2, -2});
    REQUIRE(box->max == vec3{2, 2, 2});
}

TEST_CASE("vector<shape>")
{
    std::vector<shape> shapes{};

    for (int i = 1; i < 100; i++)
        shapes.push_back(
            make_sphere(vec3{1, 1, 1}, i, make_metal(vec3{}, 0)));

    bytes::archiver ar{};
    bytes::to_bytes(ar, shapes);

    shapes = bytes::to_obj<std::vector<shape>>(ar);

    for (int i = 1; auto&& s : shapes) {
        auto box = s.bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3{1 - i, 1 - i, 1 - i});
        REQUIRE(box->max == vec3{1 + i, 1 + i, 1 + i});
        ++i;
    }
}

TEST_CASE("rects")
{
    vec2 c0{1, 2};
    vec2 c1{2, 4};
    real k = -3;

    auto xy = make_xy_rect(c0, c1, k, make_metal(vec3{}, 0));
    {
        auto box = xy.bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(c0, k - real(0.0001)));
        REQUIRE(box->max == vec3{c1, k + real(0.0001)});
    }

    auto xz = make_xz_rect(c0, c1, k, make_metal(vec3{}, 0));
    {
        auto box = xz.bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(c0[0], k - real(0.0001), c0[1]));
        REQUIRE(box->max == vec3{c1[0], k + real(0.0001), c1[1]});
    }

    auto yz = make_yz_rect(c0, c1, k, make_metal(vec3{}, 0));
    {
        auto box = yz.bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(k - real(0.0001), c0));
        REQUIRE(box->max == vec3{k + real(0.0001), c1});
    }
    std::vector<shape> v{xy, xz, std::move(yz)};
    bytes::archiver ar{};
    bytes::to_bytes(ar, v);
    auto vv = bytes::to_obj<std::vector<shape>>(ar);

    REQUIRE(vv.size() == 3u);

    {
        auto box = vv[0].bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(c0, k - real(0.0001)));
        REQUIRE(box->max == vec3{c1, k + real(0.0001)});
    }

    {
        auto box = vv[1].bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(c0[0], k - real(0.0001), c0[1]));
        REQUIRE(box->max == vec3{c1[0], k + real(0.0001), c1[1]});
    }

    {
        auto box = vv[2].bounding_box();
        REQUIRE(box);
        REQUIRE(box->min == vec3(k - real(0.0001), c0));
        REQUIRE(box->max == vec3{k + real(0.0001), c1});
    }
}

TEST_CASE("translate")
{
    auto low_corner = point3{-1, -2, -3};
    auto high_corner = point3{3, 4, 5};
    auto bx = make_box(low_corner, high_corner, make_metal(vec3{}, 0));

    auto tr = translate_shape(bx, vec3{100, 200, 300});

    bytes::archiver ar{};
    bytes::to_bytes(ar, tr);
    auto t = bytes::to_obj<shape>(ar);

    auto bbox = t.bounding_box(0, 0);
    REQUIRE(bbox);
    REQUIRE(bbox->min == point3{99, 198, 297});
    REQUIRE(bbox->max == point3{103, 204, 305});
}

TEST_CASE("rotate")
{
    auto low_corner = point3{0, 0, 0};
    auto high_corner = point3{2, 3, 4};
    auto bx = make_box(low_corner, high_corner, make_metal(vec3{}, 0));
    
    auto r = rotate_y_shape(bx, std::numbers::pi);

    bytes::archiver ar{};
    bytes::to_bytes(ar, r);
    auto rr = bytes::to_obj<shape>(ar);

    auto bbox = rr.bounding_box(0, 0);
    REQUIRE(bbox);
    REQUIRE(bbox->min.x == Catch::Approx(-2.0));
    REQUIRE(bbox->min.y == Catch::Approx(0.0).margin(1.0e-10));
    REQUIRE(bbox->min.z == Catch::Approx(-4.0));

    REQUIRE(bbox->max.x == Catch::Approx(0.0).margin(1.0e-10));
    REQUIRE(bbox->max.y == Catch::Approx(3.0));
    REQUIRE(bbox->max.z == Catch::Approx(0.0).margin(1.0e-10));
}
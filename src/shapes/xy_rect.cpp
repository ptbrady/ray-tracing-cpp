#include "xy_rect.hpp"
#include "shape.hpp"

template struct rect<2>;

shape make_xy_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat)
{
    return {xy_rect{corner0, corner1, k, mat}};
}

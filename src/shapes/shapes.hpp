#pragma once

#include "shape.hpp"

#include "sphere.hpp"
#include "moving_sphere.hpp"
#include "xy_rect.hpp"
#include "yz_rect.hpp"
#include "xz_rect.hpp"
#include "box.hpp"
#include "translate.hpp"
#include "rotate.hpp"
#include "constant_medium.hpp"

#include "shape_tag_invoke.hpp"

#pragma once

#include "materials/material.hpp"
#include "shape.hpp"

struct constant_medium {
    shape boundary;
    material phase_function;
    real neg_inv_density;

    std::optional<hit_record> hit(const ray& r, real t_min, real t_max) const;

    std::optional<aabb> bounding_box(real time_min, real time_max) const;

    friend void
    tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>, bytes::archiver&, const constant_medium&);

    friend void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver&, constant_medium&);
};
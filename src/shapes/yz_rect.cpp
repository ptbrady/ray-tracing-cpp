#include "yz_rect.hpp"
#include "shape.hpp"

template struct rect<0>;

shape make_yz_rect(const point2& corner0,
                   const point2& corner1,
                   real k,
                   const material& mat)
{
    return {yz_rect{corner0, corner1, k, mat}};
}

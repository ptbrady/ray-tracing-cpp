#include "constant_medium.hpp"
#include "shapes.hpp"
#include "random.hpp"
#include <limits>

static constexpr auto inf = std::numeric_limits<real>::max();
static constexpr auto neg_inf = std::numeric_limits<real>::lowest();

shape make_constant_medium(const shape& s, const texture& t, real density)
{
    return {constant_medium{s, make_isotropic(t), -1 / density}};
}

std::optional<hit_record>
constant_medium::hit(const ray& r, real t_min, real t_max) const
{
    auto h1 = boundary.hit(r, neg_inf, inf);
    if (!h1) return std::nullopt;
    real t1 = h1->t;

    auto h2 = boundary.hit(r, t1+0.0001, inf);
    if (!h2) return std::nullopt;
    real t2 = h2->t;

    // clip hit distances
    t1 = std::max(t1, t_min);
    t2 = std::min(t2, t_max);

    if (t1 > t2) return std::nullopt;

    t1 = std::max(t1, real(0));

    const real ray_length = glm::length(r.direction);
    const real dist_inside_boundary = (t2 - t1) * ray_length;
    const real hit_dist = neg_inv_density * std::log(pick());

    if (hit_dist > dist_inside_boundary) return std::nullopt;

    // construct the actual hit_record to retrun.  Both the normal and ray_outside flag
    // are arbitrary
    h1->t = t1 + hit_dist / ray_length;
    h1->point = r.position(h1->t);
    h1->mat = phase_function;

    return h1;
}

std::optional<aabb> constant_medium::bounding_box(real time_min, real time_max) const
{
    return boundary.bounding_box(time_min, time_max);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_bytes>,
                bytes::archiver& ar,
                const constant_medium& s)
{
    bytes::to_bytes(ar, s.boundary);
    bytes::to_bytes(ar, s.phase_function);
    bytes::to_bytes(ar, s.neg_inv_density);
}

void tag_invoke(taggie::tag_t<bytes::cpo::to_obj>, bytes::archiver& ar, constant_medium& s)
{
    s.boundary = bytes::to_obj<shape>(ar);
    s.phase_function = bytes::to_obj<material>(ar);
    s.neg_inv_density = bytes::to_obj<real>(ar);
}
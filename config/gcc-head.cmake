set(CMAKE_C_COMPILER gcc-head CACHE STRING "C Compiler")
set(CMAKE_CXX_COMPILER g++-head CACHE STRING "C++ Compiler")
set(CMAKE_CXX_FLAGS_DEBUG '-Wall -fsanitize=address' CACHE STRING "Debug flags for g++")

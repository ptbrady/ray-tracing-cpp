--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;
    vec2 -> construct a glm::vec2 object

    solid_color -> construct a solid color texture
    checkered -> construct a checkered texture from two textures
    noise -> construct a perlin noise texture

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material
    diffuse_light -> construct a diffuse light material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object
config = configuration.new()

-- maximum recursion depth for rays
config.max_depth = 50
-- more samples for better anit-aliasing
config.samples_per_pixel = 100

-- buffering for parallel speedyness.
-- Not so sure the complexity introduced by this option is worth it
config.pixels_per_process = 4

aspect_ratio = 1
width = 300
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}

-- black backgound since we are relying on our light sources 
-- config.background_color = vec3(0, 0, 0)

red = solid_color(0.65, 0.05, 0.05)
white = solid_color(0.73, 0.73, 0.73)
green = solid_color(0.12, 0.45, 0.15)

--[[
cornell box
config:add_yz_rect{
    lower_corner = vec2(0, 0),
    upper_corner = vec2(555, 555),
    plane_coordinate = 555,
    material = lambertian(green)
}

config:add_yz_rect{
    lower_corner = vec2(0, 0),
    upper_corner = vec2(555, 555),
    plane_coordinate = 0,
    material = lambertian(red)
}

-- lightsource
config:add_xz_rect{
    lower_corner = vec2(213, 227),
    upper_corner = vec2(343, 332),
    plane_coordinate = 554,
    material = diffuse_light(color(15, 15, 15))
}

config:add_xz_rect{
    lower_corner = vec2(0, 0),
    upper_corner = vec2(555, 555),
    plane_coordinate = 555,
    material = lambertian(white)
}

config:add_xz_rect{
    lower_corner = vec2(0, 0),
    upper_corner = vec2(555, 555),
    plane_coordinate = 0,
    material = lambertian(white)
}

config:add_xy_rect{
    lower_corner = vec2(0, 0),
    upper_corner = vec2(555, 555),
    plane_coordinate = 555,
    material = lambertian(white)
}

]] 

-- boxes
config:add_box{
    lower_corner = vec(-2, -2, -2),
    upper_corner = vec(2, 2, 2),
    material = lambertian(red)
}
--[[
config:add_xy_rect{
    lower_corner = vec2(-2, -2),
    upper_corner = vec2(2, 2),
    plane_coordinate = 2,
    material = lambertian(green)
}
]]


config:camera{
    vertical_field_of_view = math.rad(40),
    aspect_ratio = aspect_ratio,
    look_from = point(10, 2, 3),
    look_at = vec(0, 0, 0),
    view_up = vec(0, 1, 0),
    aperature = 0.0,
    focus_distance = 10.0,
    time_min = 0.0,
    time_max = 1.0
}

return config
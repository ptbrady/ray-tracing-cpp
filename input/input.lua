--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;

    vec2 -> construct a glm::vec2 object;

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material
    diffuse_light -> construct a diffuse light material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object

config = configuration.new()

aspect_ratio = 16/9
width = 384
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}


-- add shapes        
config:add_moving_sphere{
    center_a = point(0.0, 0.0, -1.0), 
    center_b = point(0.0, 0.5, -1.0),
    time_a = 0.0,
    time_b = 1.0,
    radius = 0.5,
    material = lambertian(solid_color(0.1, 0.2, 0.5)),
    -- material = dielectric(1.5), -- glass
}
-- large backgound sphere
config:add_sphere{
    center = point(0, -100.5, -1),
    radius = 100,
    material = lambertian(solid_color(0.8, 0.8, 0.0))
}

-- some metal spheres
config:add_sphere{
    center = point(1, 0, -1),
    radius = 0.5,
    material = metal(color(0.8, 0.6, 0.2), 0.1),
}

config:add_sphere{
    center = point(-1, 0, -1),
    radius = 0.5,
    -- material = metal(color(0.8, 0.8, 0.8), 0.8),
    material = dielectric(1.6),
}

config:add_sphere{
    center = point(-1, 0, -1),
    radius = -0.45,
    -- material = metal(color(0.8, 0.8, 0.8), 0.8),
    material = dielectric(1.6),
}

--[[
R = math.cos(math.pi/4);
config:add_sphere{
    center = point(-R, 0, -1),
    radius = R,
    material = lambertian(color(0, 0, 1)),
}
config:add_sphere{
    center = point(R, 0, -1),
    radius = R,
    material = lambertian(color(1, 0, 0))
}
]]

look_from = point(3, 3, 2)
look_at = point(0, 0, -1)

config:camera{
    vertical_field_of_view = math.rad(20),
    aspect_ratio = aspect_ratio,
    look_from = look_from,
    look_at = look_at,
    view_up = vec(0, 1, 0),
    aperature = 2.0,
    focus_distance = vec3.distance(look_from, look_at),
    time_min = 0.0,
    time_max = 0.5
}

return config
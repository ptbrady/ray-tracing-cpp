--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object
config = configuration.new()

-- maximum recursion depth for rays
config.max_depth = 50
-- more samples for better anit-aliasing
config.samples_per_pixel = 100

-- buffering for parallel speedyness.
-- Not so sure the complexity introduced by this option is worth it
config.pixels_per_process = 4

aspect_ratio = 16/9
width = 384*2
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}


-- add a single sphere with an earth texture
config:add_sphere{
    center = point(0, 0, 0),
    radius = 2,
    material = lambertian(
        image("resource/earthmap.jpg"))
}



config:camera{
    vertical_field_of_view = math.rad(20),
    aspect_ratio = aspect_ratio,
    look_from = point(13, 2, 3),
    look_at = vec(0, 0, 0),
    view_up = vec(0, 1, 0),
    aperature = 0.0,
    focus_distance = 10.0,
    time_min = 0.0,
    time_max = 1.0
}

return config
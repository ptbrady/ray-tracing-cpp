--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;
    vec2 -> construct a glm::vec2 object

    solid_color -> construct a solid color texture
    checkered -> construct a checkered texture from two textures
    noise -> construct a perlin noise texture

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material
    diffuse_light -> construct a diffuse light material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object
config = configuration.new()

-- maximum recursion depth for rays
config.max_depth = 50
-- more samples for better anit-aliasing
config.samples_per_pixel = 500

-- buffering for parallel speedyness.
-- Not so sure the complexity introduced by this option is worth it
config.pixels_per_process = 4

aspect_ratio = 16/9
width = 384*2
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}

config.background_color = vec3(0, 0, 0)

noise_texture = noise(256, 4);

-- simple case with two spheres up-close
config:add_sphere{
    center = point(0, -1000, 0),
    radius = 1000,
    material = lambertian(noise_texture)
}

config:add_sphere{
    center = point(4, 5.5, -2),
    radius = 2,
    material = lambertian(noise_texture)
}


config:add_xy_rect{
    lower_corner = vec2(3, 1),
    upper_corner = vec2(5, 3),
    plane_coordinate = -2,
    material = diffuse_light(vec3(4, 4, 4))
    -- material = lambertian(solid_color(0, 0, 1))
}

config:add_sphere{
    center = point(-8, 8, -2),
    radius = 2,
    material = diffuse_light(vec3(4, 4, 4))
}



config:camera{
    vertical_field_of_view = math.rad(20),
    aspect_ratio = aspect_ratio,
    look_from = point(26, 3, 6),
    look_at = vec(0, 2, 0),
    view_up = vec(0, 1, 0),
    aperature = 0.0,
    focus_distance = 10.0,
    time_min = 0.0,
    time_max = 1.0
}

return config
--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object
config = configuration.new()

-- maximum recursion depth for rays
config.max_depth = 50
-- more samples for better anit-aliasing
config.samples_per_pixel = 100

-- buffering for parallel speedyness.
-- Not so sure the complexity introduced by this option is worth it
config.pixels_per_process = 4

aspect_ratio = 16/9
width = 384*2
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}


-- start with ground material
config:add_sphere{
    center = point(0, -1000, 0),
    radius = 1000,
    material = lambertian(
        checkered(
            solid_color(0.2, 0.3, 0.1), solid_color(0.9, 0.9, 0.9)))
}

-- we will need some randomness
math.randomseed(os.time())

function random_range(a, b)
    return (b-a) * math.random() + a
end

function random_color(...)
    local y0, y1 = ...
    if y0 == nil or y1 == nil then
        return color(math.random(), math.random(), math.random())
    else
        return color(random_range(y0, y1), random_range(y0, y1), random_range(y0, y1))
    end
end

for a = -13, 13 do
    for b = -13, 13 do
        
        center = point(a + 0.9*math.random(), 0.2, b + 0.9*math.random())
        if vec3.distance(center, point(4, 0.2, 0)) > 0.9 then
            choose_material = math.random()

            if choose_material < 0.8 then
                -- lambertian moving spheres
                config:add_moving_sphere{
                    time_a = 0.0,
                    time_b = 1.0,
                    center_a = center,
                    center_b = center + vec(0, random_range(0, 0.5), 0),
                    radius = 0.2,
                    material = lambertian(solid_color(random_color()*random_color())),
                }
            elseif choose_material < 0.95 then
                -- metal
                config:add_sphere{
                    center = center,
                    radius = 0.2,
                    material = metal(random_color(0.5, 1.0), random_range(0, 0.5)),
                }
            else
                -- glass
                config:add_sphere{
                    center = center,
                    radius = 0.2,
                    material = dielectric(1.5),
                }
            end
        end
    end
end

-- largish center sphere
config:add_sphere{
    center = point(0, 1, 0),
    radius = 1.0,
    material = dielectric(1.5),
}

config:add_sphere{
    center = point(-4, 1, 0),
    radius = 1.0,
    material = lambertian(solid_color(0.4, 0.2, 0.1)),
}

config:add_sphere{
    center = point(4, 1, 0),
    radius = 1.0,
    material = metal(color(0.7, 0.6, 0.5), 0.0),
}

config:camera{
    vertical_field_of_view = math.rad(20),
    aspect_ratio = aspect_ratio,
    look_from = point(13, 2, 3),
    look_at = vec(0, 0, 0),
    view_up = vec(0, 1, 0),
    aperature = 0.0,
    focus_distance = 10.0,
    time_min = 0.0,
    time_max = 1.0
}

return config
--[[
    top level functions injected by raytracer are:

    configuration.new() -> initialize a new raytracing configuration object

    vec -> construct a glm::vec3 object
    color -> descriptive alias for vec;
    point -> descriptive alias for vec;
    vec2 -> construct a glm::vec2 object

    solid_color -> construct a solid color texture
    checkered -> construct a checkered texture from two textures
    noise -> construct a perlin noise texture

    lambertian -> construct a lambertian material
    metal -> construct a metal material
    dielectric -> construct a dielectric material
    diffuse_light -> construct a diffuse light material

    methods on the raytracing config object:
    config:image_size{nx = int, ny = int}
    config:add_sphere{center = point, radius = float, material = material}
    config:camera{
        vertical_field_of_view = angle (radians),
        aspect_ratio = float}
]]
-- initialize a new raytracing configuration object
config = configuration.new()

-- maximum recursion depth for rays
config.max_depth = 50
-- more samples for better anit-aliasing
config.samples_per_pixel = 1000

-- buffering for parallel speedyness.
-- Not so sure the complexity introduced by this option is worth it
config.pixels_per_process = 4

aspect_ratio = 1
width = 800
-- config:image_size(width, math.floor(width/aspect_ratio))
config:image_size{nx = width,
                  ny = math.floor(width/aspect_ratio)}

-- black backgound since we are relying on our light sources 
config.background_color = color(0, 0, 0)

-- we will need some randomness
math.randomseed(os.time())

function random_range(a, b)
    return (b-a) * math.random() + a
end

function random_color(...)
    local y0, y1 = ...
    if y0 == nil or y1 == nil then
        return color(math.random(), math.random(), math.random())
    else
        return color(random_range(y0, y1), random_range(y0, y1), random_range(y0, y1))
    end
end
random_point = random_color

ground = lambertian(solid_color(0.48, 0.83, 0.53))
boxes_per_side = 20
for i = 1, boxes_per_side do
    for j = 1, boxes_per_side do
        w = 100
        x0 = (i-1)*w - 1000
        z0 = (j-1)*w - 1000
        y0 = 0
        x1 = x0 + w;
        y1 = random_range(1, 91)
        z1 = z0 + w;
        config:add_box{
            lower_corner = point(x0, y0, z0),
            upper_corner = point(x1, y1, z1),
            material = ground
        }
    end
end


-- lightsource
config:add_xz_rect{
    lower_corner = vec2(123, 147),
    upper_corner = vec2(423, 412),
    plane_coordinate = 554,
    material = diffuse_light(color(7, 7, 7))
}

config:add_moving_sphere{
    center_a = point(400, 400, 200),
    center_b = point(430, 400, 200),
    time_a = 0,
    time_b = 1,
    radius = 50,
    material = lambertian(solid_color(0.7, 0.3, 0.1))
}

config:add_sphere{
    center = point(260, 150, 45),
    radius = 50,
    material = dielectric(1.5)
}
config:add_sphere{
    center = point(0, 150, 145),
    radius = 50,
    material = metal(color(0.8, 0.8, 0.9), 10)
}

boundary = make_sphere{
    center = point(360, 150, 145),
    radius = 70,
    material = dielectric(1.5)
}
config:add_shape(boundary)

config:add_constant_medium{
    shape = boundary,
    density = 0.2,
    texture = solid_color(0.2, 0.4, 0.9)
}

config:add_constant_medium{
    shape = make_sphere{
        center = point(0, 0, 0),
        radius = 5000,
        material = dielectric(1.5)
    },
    density = 0.0001,
    texture = solid_color(1, 1, 1)
}

config:add_sphere{
    center = point(400, 200, 400),
    radius = 100,
    material = lambertian(image("resource/earthmap.jpg"))
}

config:add_sphere{
    center = point(220, 280, 300),
    radius = 80,
    material = lambertian(noise(256, 0.1))
}

white = lambertian(solid_color(0.73, 0.73, 0.73))

for ns = 1, 1000 do
    bx = make_sphere{
        center = random_point(0, 165),
        radius = 10,
        material = white
    }
    config:add_translate_shape{
        shape = make_rotate_shape{
            shape = bx,
            axis = 'y',
            angle = math.rad(15)
        },
        displacement = vec(-100, 270, 395)
    }
end

config:camera{
    vertical_field_of_view = math.rad(40),
    aspect_ratio = aspect_ratio,
    look_from = point(478, 278, -600),
    look_at = vec(278, 278, 0),
    view_up = vec(0, 1, 0),
    aperature = 0.0,
    focus_distance = 10.0,
    time_min = 0.0,
    time_max = 1.0
}

return config